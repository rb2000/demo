# Demo Conference Room Booking Application
 
#### Stack: 
Spring Boot v2.4.2, Spring v5.3.3, java 1.8, H2 in-memory db , Spring Security, Docker  (see maven pom.xml for details) 

#### Running:
- `mvn clean spring-boot:run`

# **TESTS and ASSIGNMENTS**:
- `mvn clean test`
This will run repository and API tests.
 
The test files _**"Assignment_1_1_Test.java**", **"Assignment_1_2_Test.java"**,  **"Assignment_1_3_Test.java"**   and  **"Assignment_1_4_Test.java"**_ correspond to the assigned tasks on the challenge doc.
 
#### Dropping all the bookings:
If you want to drop the bookings that are inserted at boot time (as well as all the bookings you might have made) you can use the endpoint ( as user 'application') `/api/v1/admin/cleanDatabase`
```
curl --request GET \
  --url http://localhost:8080/api/v1/admin/cleanDatabase \
  -H 'Accept: application/json' -u 'application:somepassword'
```



#### Docker Image
Build and push image to local docker repo (docker daemon needs to be running on your machine)

- To build the image
` mvn clean compile jib:dockerBuild `

- to run the image:
`docker run -p 127.0.0.1:8080:8080/tcp demo`

Authorized users can book a room if the slot is free ( a “reservation” object is created ).
Reserved rooms must be confirmed with a time range, if not the reservation will be set to “timedout”.
_You can add more users/reservations/rooms to the system by editting the class DatabaseInitializer_


#### Features:
 - All controller methods secured (spring security 5,  roles ADMIN, EMPLOYEE, APPLICATION )
 - Scheduler (need to call app with spring profile set to “scheduler” that runs at 00:00  and sets pending reservations status to TIMED_OUT and removes unconfirmed reservations if the time-to-live has expired.   It runs at 00:30 too every night and deletes the unconfirmed reservations (can be changed in the cron expression )
   By default the scheduler is disabled. You can enable it when you start the application passing the scheduler profile: 

  ` mvn spring-boot:run -Dspring-boot.run.profiles=scheduler`
  
 - Authorized users can book a room if the slot is free (a “reservation” object is created).
 - Reserved rooms must be confirmed with a time range, if not the reservation will be set to “timedout”.
 - User with the the APPLICATION role can remove reservations or change their status (expired,”pending”,etc. )
 - Rooms are bookable for max. 24h,  the end of the booking range being midnight.

#### Default users
The default users are added at startup to the system by the helper classs DatabaseInitializer: “employee1”, “employee2”, “employee3”, “admin”, “application” and all passwords are set to “somepassword”   ( http basic  authorization )
| user name | password | team | role |
| ------ | ------ | ------ | ------ |
| employee1 | somepassword | teamOne | EMPLOYEE |
| employee2 | somepassword | teamOne | EMPLOYEE |
| employee3 | somepassword | teamOne | EMPLOYEE |
| admin | somepassword | adminTeam | ADMIN |
| application | somepassword | applicationTeam | APPLICATION |
 
**Following Conference Rooms are also added:**
| floor | room number | hourly fee | currency | has video | has drawing board | capacity  |
| ------ | ------ | ------ | ------ | ------ | ------ |------ |
| 1 | 123A | 120 | EURO | false | false | 11 |
| 2 | 201 | 345.34 | EURO | false | false | 17 |
| 3 | 306 | 50 | EURO | false | false | 3 |

The fees are calculated in a minute basis.


** Reservations are added to the system at boot time:**
| Id | room number | reserved by | from | until | status| 
| ------ | ------ | ------ | ------ | ------ | ------ |
| 1 | 123A | employee1 |tomorrow 04:00 | tomorow 05:00 | PENDING | 
| 2 | 306 | employee1 | tomorrow 00:00 | tomorow 01:00 | PENDING | 
| 3 | 201 | employee1 | tomorrow 02:00 | tomorow 03:00 | PENDING | 
| 4 | 201 | employee3| tomorrow 06:00 | tomorow 07:00 | PENDING | 

#### H2 Console:
In order to use the h2 console with spring 5 security, the app needs to be called with the the config for 
that profile as 

`mvn spring-boot:run -Dspring-boot.run.profiles=h2-console`
You can then reach the console at 

`http://localhost:8080/h2-console`     (jdbc url:"jdbc:h2:mem:dnbdemo" , user:"sa",pwd:"")

###  Get all Reservations
`ENDPOINT  /api/v1/reservations/all`

```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/all \
   -H 'Accept: application/json' -u 'application:somepassword'
```


### Booking a room:
- first check if the room is available on the date range using the endpoint:
`/api/v1/rooms/availability/{flooNumber}/{beginDate}/{endDate}`
For example you can use curl like
```
curl --request GET \
  --url http://localhost:8080/api/v1/rooms/availability/1/31-01-2021%2000:00/30-02-2021%2000:00  \
    -H 'Accept: application/json' -u 'application:somepassword'
```

 - And then book a room in a date that is not reserved at the endpoint
```
curl --request GET \
--url http://localhost:8080/api/v1/rooms/book/201/13-02-2022%2010:00/13-02-2022%2012:00 \
   -H 'Accept: application/json' -u 'application:somepassword'
```

### Confirm a reservation

`/api/v1/reservations/confirm/{id}`

```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/confirm/1 \
   -H 'Accept: application/json' -u 'application:somepassword'
```


Only the user who booked the room can confirmm a reservation. If other user tries to confirm it an exception 
is thrown :
![unauthorize2d](/uploads/850afefefc11e14ffe8be9a45defadd7/unauthorize2d.png)

### Invoices can be created by an user with the role APPLICATION at the endpoint 

` /api/v1/invoices/team/{teamName}/{month}/{year}`

**Make sure you have CONFIRMED the reservation before you call the invoices,** bookings with their statuses set to PENDING will not show in the invoices.  You can only confirm a reservation you made.
 The default generated reservations on the database at startup are for the users "employee1" and "employee2". You need to call curl as one of those users (for example  employee1:somepassword instead of application:somepasword), confirm the reservation at the endpoint _/api/v1/reservations/confirm/{reservationId}_


```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/confirm/1 \
   -H 'Accept: application/json' -u 'employee1:somepassword'
```
Do the same for another reservation, so that the invoice will contain the sum of the 2 reservation fees:

```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/confirm/2 \
   -H 'Accept: application/json' -u 'employee1:somepassword'
```


  then change a user with APPLICATION role (userName is "application", pwd is "somepassword" )
and then call the invoices like :

```
curl --request GET \
  --url http://localhost:8080/api/v1/invoices/team/teamOne/2/2021 \
   -H 'Accept: application/json' -u 'application:somepassword'
```
### Show all rooms
Show all rooms in system.
`/api/v1/rooms/all`

```
 curl --request GET \
  --url http://localhost:8080/api/v1/rooms/all \
   -H 'Accept: application/json' -u 'application:somepassword'
```

### Search a room in a specific floor, room with video or drawing board
`/api/v1/rooms//find/{floorNumber}/{hasVideo}/{hasDrawingBoard}`

```
curl --request GET \
  --url http://localhost:8080/api/v1/rooms/find/1/false/false \
   -H 'Accept: application/json' -u 'application:somepassword'
```
### Delete a single reservation 

(user must have APPLICATION role)

`/api/v1/reservations/1 `      ( use a delete request )
```
curl --request DELETE \
  --url http://localhost:8080/api/v1/reservations/2 \
   -H 'Accept: application/json' -u 'application:somepassword'
```

### Delete unconfirmed reservations 
(user must have APPLICATION role)
`api/v1/reservations/deleteAllUnconfirmed`
Will delete reservations that have a status set to TIMED_OUT.

`ENDPOINT  /api/v1/reservations/deleteAllUnconfirmed`

```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/deleteAllUnconfirmed \
   -H 'Accept: application/json' -u 'application:somepassword'
```
Run the time out check first,(example bellow) which will set if reservations were not confirmed on time and will set their statuses to TIMED_OUT, so that they can be collected for deletion by the /deleteAllUnconfirmed  endpoint above
```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/timeoutCheck \
   -H 'Accept: application/json' -u 'application:somepassword'
```
### Book a room in an already taken slot 
The system checks if the reservation times overlap and if the status of the already booked 
room on that time is TIMED_OUT or CONFIRMED,ther the room will be booked. Other wise it will 
not be booked and a message is set to the user:
Example:  there is already a pending reservation on the 4th of february between midnight and 00:00. When an user tries to book a room on that date between 00:30 and 00:40 he will receive a message showing that 
that room is not available:

```
curl --request GET \
  --url http://localhost:8080/api/v1/rooms/book/306/04-02-2021%2000:30/04-02-2021%2000:40 \
   -H 'Accept: application/json' -u 'application:somepassword'
```
Result:
![booking](/uploads/ee508097a831d84672677d02a56658c9/booking.png)


  For more information refer to the file RoomBooking.doc .
 

## ASSIGNMENT
1. [x] Employee should confirm the arrival to the conference room.
`/api/v1/reservations/reservation/confirm-arrival/{reservationId}/{employeeName}`

```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/confirm-arrival/1/employee1 \
   -H 'Accept: application/json' -u 'application:somepassword'
```

 
2. [x] The system should not allow more people than the maximum capacity of the office space. 
Enter the previous request until the room capacity is exceeded, it stops  increasing the number of attendants in the room and returns a message

3. [x] If the reservation is not confirmed within 10% of reserved time the room is returned to the pool. The maximum reservation is 1 day.
The cron job can remove timed out reservations automatically, you can set how often itshould run. It its possible to call the method that sets the status of not confirmed reservations to TIMED_OUT ( `setExpireReservationsStatus()` ), so that new reservations can be booked on that slot.  Reservations can be made for the time range 00:00 until 23:59 of a day.  The cron job is disable by default, it can be activated using the spring boot paramater "scheduler" when calling maven.
To remove unconfirmed reservations manually, do:
1. Set the unconfirmed booking statuses to "TIMED_OUT"(it wil search the db for timed out reservations and set their statuses to timed out)
` curl --request GET   --url http://localhost:8080/api/v1/reservations/timeoutCheck    -H 'Accept: application/json' -u 'application:somepassword'`

2. Call the remove timed out reservations endpoint:
```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/deleteAllUnconfirmed \
   -H 'Accept: application/json' -u 'application:somepassword'
```
 
4. [x] The system should not allow more people than the maximum capacity of the office space. 
 
```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/confirm-arrival/1/employee1 \
   -H 'Accept: application/json' -u 'application:somepassword'
```
Enter this request until the room capacity is exceeded, it stops  increasing the number of attendants in the room and returns a message



5. [x] Each floor should have a display board showing upcoming conference room reservations.
`/api/v1/reservations/fromNow/{floorNumber}`

```
 curl --request GET \
  --url http://localhost:8080/api/v1/rooms/availability/1 \
   -H 'Accept: application/json' -u 'application:somepassword'
```


6. [x] The system should support a monthly team fee model. For example, reservations of the employees belonging to the same team are invoiced as one fee entry
` /api/v1/invoices/team/{teamName}/{month}/{year}`
Make sure you have CONFIRMED the reservation before you call the invoices, bookings with their statuses set to PENDING 
will not show in the invoices.  You can only confirm a reservation you made. The default generated reservations on the database at startup are for the users "employee1" and "employee2". You need to login as one of those users ( pwd: "somepassword") ,confirm the revervation at the endpoint  http://localhost:8080/api/v1/reservations/confirm/{reservationId}
```
curl --request GET \
  --url http://localhost:8080/api/v1/reservations/confirm/1 \
   -H 'Accept: application/json' -u 'employee1:somepassword'
```

  then change a user with APPLICATION role (userName is "application", pwd is "somepassword" )
and then call the invoices like :

```
curl --request GET \
  --url http://localhost:8080/api/v1/invoices/team/teamOne/2/2021 \
   -H 'Accept: application/json' -u 'application:somepassword'
```
### Tests
Tests can be performed by the maven command 
 ` mvn clean test `

 It will start some repository unit tests as well as some mock API tests.

 To generate ascii documentation you can call
 ` mvn clean  package -Pdocs`
 which will activate the maven doc profile and generate the snippets in the "target/generated-snippets" directory , and the index.html file in the "target/generated-docs" directory.


 
### ALL ENDPOINTS 

##### GET
api/v1/reservations/all
/api/v1/reservations/fromNow/{floorNumber}
/api/v1/reservations/{id}
/api/v1/reservations/deleteAllUnconfirmed  (only application allowed)
/api/v1/reservations/confirmation/{reservationId}/{employeeName}
/api/v1/reservations/timeoutCheck 

##### POST
/api/v1/reservations/reservation
##### PUT
/api/v1/reservations/{id}
##### DELETE
/api/v1/reservations/{id}
##### PATCH
/api/v1/reservations/{id}

#### INVOICE CONTROLLER 
##### GET
/api/v1/invoices/singleuser/{userName}/{month}/{year} 

/api/v1/invoices/team/{teamName}/{month}/{year}

#### USERS CONTROLLER  
##### GET
/api/v1/users/{id}
/api/v1/users/allusers
/api/v1/users/whoami

#### ROOM CONTROLLER 
##### GET
/api/v1/rooms/availability/{flooNumber}/{beginDate}/{endDate}
/api/v1/rooms/book/{roomNumber}/{beginDate}/{endDate}
/api/v1/rooms/find/{floorNumber}/{hasVideo}/{hasDrawingBoard}





