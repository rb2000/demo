package tech.rvidal.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Rule;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class Assignment_1_1_Test extends RepositoryTestBase {

  @Rule
  public final JUnitRestDocumentation restDocumentation =
      new JUnitRestDocumentation("target/generated-snippets");

  @BeforeAll
  void setUp() throws Exception {
    cleanDatabase();
    log.info("Creating users and rooms ...");
    super.createEntities();
  }
  @AfterEach
  void clean() {
    cleanDatabase();
  }
  @BeforeEach
  void setup(
      WebApplicationContext webApplicationContext,
      RestDocumentationContextProvider restDocumentation) {
    super.setupBeforeEach(webApplicationContext, restDocumentation);
  }

  /**
   * ASSIGNMENT ONE: "As administrator, I want to produce the monthly invoice for each team."
   *
   * <p>  Fetches an invoice for a team. Two rooms are booked by distinct users(who belong to the same
   * team). The invoice result will contain the total sum of the fees for that team.
   *
   * @throws Exception
   */
  @Test
  @WithMockUser(
      username = "employee_1",
      authorities = {"ADMIN", "APPLICATION"})
  void whenFetchingInvoiceForConfirmedReservations_ThenOk() throws Exception {
    new HttpSessionCsrfTokenRepository();
    /* create a reservation */
    bookRoom(
        firstDayNextMonth.plusHours(4),
        firstDayNextMonth.plusHours(6).plusMinutes(27),
        conferenceRoomOne,
        simpleUserOne,
        "CONFIRMED");
    bookRoom(
        firstDayNextMonth.plusHours(7),
        firstDayNextMonth.plusHours(8),
        conferenceRoomOne,
        simpleUserTwo,
        "CONFIRMED");
    bookRoom(
        firstDayNextMonth.plusHours(9),
        firstDayNextMonth.plusHours(10),
        conferenceRoomOne,
        simpleUserThree,
        "CONFIRMED");
    bookRoom(
        firstDayNextMonth.plusHours(11),
        firstDayNextMonth.plusHours(12),
        conferenceRoomOne,
        simpleUserTwo,
        "PENDING");
    log.info(
        "Performing GET API call to endpoint '/api/v1/reservations/team/{teamName}/{month}/{year}'");
    this.mockMvc
        .perform(
            MockMvcRequestBuilders.get(
                    SERVER + "/api/v1/invoices/team/{teamName}/{month}/{year}",
                    "teamOne",
                    firstDayNextMonth.getMonth().getValue(),
                    "2021")
                .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.sum").value("414.0"))
        .andExpect(jsonPath("$.reservations[0].totalFee").value("294.00"))
        .andExpect(jsonPath("$.reservations[1].totalFee").value("120.00"))
        .andExpect(jsonPath("$.totalTimeInMinutes").value("207.0"));
  }
}
