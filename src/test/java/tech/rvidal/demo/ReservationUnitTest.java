package tech.rvidal.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import tech.rvidal.demo.domain.Reservation;
import tech.rvidal.demo.domain.ReservationStatus;
import tech.rvidal.demo.exceptions.RoomNotAvailableOnDateException;
import tech.rvidal.demo.repository.ConferenceRoomRepository;
import tech.rvidal.demo.repository.ConferenceRoomUserRepository;
import tech.rvidal.demo.repository.ReservationRepository;
import tech.rvidal.demo.service.ReservationService;

import java.time.LocalDateTime;
import java.time.LocalTime;

import static java.time.LocalDate.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ReservationUnitTest extends RepositoryTestBase {

  @Autowired private ConferenceRoomUserRepository userRepository;
  @Autowired private ConferenceRoomRepository roomRepository;
  @Autowired private ReservationRepository reservationRepository;
  @Autowired private ReservationService reservationService;
  @Autowired private MockMvc mockMvc;
  private LocalTime noon = LocalTime.NOON;

  @BeforeEach
  void init() {
    cleanDatabase();
    createEntities();
  }

  @AfterEach
  void clean() {
    cleanDatabase();
  }
  /** creates 3 reservations, their time range not overlapping. Test should succeed */
  @Test
  void whenReservationWithinFreeSlot_ThenSuccess() {
    LocalDateTime tomorrowNoon = LocalDateTime.of(now().plusDays(1), noon);

    log.info("Creating reservation at 12:00:01");
    Reservation reservationNoon =
        Reservation.builder()
            .reservationStart(tomorrowNoon.plusSeconds(1))
            .reservationEnd(tomorrowNoon.plusHours(2))
            .conferenceRoom(conferenceRoomOne)
            .conferenceRoomUser(simpleUserOne)
            .status(ReservationStatus.PENDING)
            .build();
    Reservation fromDB = reservationService.createReservation(reservationNoon);
    assertThat(fromDB).isNotNull();
    assertThat(fromDB.getConferenceRoom()).isNotNull();
    assertThat(fromDB.getConferenceRoomUser()).isNotNull();
    assertThat(reservationRepository.count()).isEqualTo(1L);

    log.info("Creating reservation at 14:00:01");
    Reservation reservationTwoOclock =
        Reservation.builder()
            .reservationStart(tomorrowNoon.plusHours(2).plusSeconds(1))
            .reservationEnd(tomorrowNoon.plusHours(4))
            .conferenceRoom(conferenceRoomOne)
            .conferenceRoomUser(simpleUserOne)
            .status(ReservationStatus.PENDING)
            .build();
    fromDB = reservationService.createReservation(reservationTwoOclock);
    assertThat(fromDB).isNotNull();
    assertThat(fromDB.getConferenceRoom()).isNotNull();
    assertThat(fromDB.getConferenceRoomUser()).isNotNull();
    assertThat(reservationRepository.count()).isEqualTo(2L);

    log.info("Creating reservation at 16:00:01");
    Reservation reservationFourOclock =
        Reservation.builder()
            .reservationStart(tomorrowNoon.plusHours(4).plusSeconds(1))
            .reservationEnd(tomorrowNoon.plusHours(6))
            .conferenceRoom(conferenceRoomOne)
            .conferenceRoomUser(simpleUserTwo)
            .status(ReservationStatus.PENDING)
            .build();
    fromDB = reservationService.createReservation(reservationFourOclock);
    assertThat(fromDB).isNotNull();
    assertThat(fromDB.getConferenceRoom()).isNotNull();
    assertThat(fromDB.getConferenceRoomUser()).isNotNull();
    assertThat(reservationRepository.count()).isEqualTo(3L);
  }

  /**
   * creates 2 reservations, the time range of the second one overlapping with the range of the
   * first one. Test should fail, as a RoomNotAvailableOnDateException will be thrown
   */
  @Test
  void whenReservationWithinTakenSlot_ThenException() {
    assertThat(reservationRepository.count()).isEqualTo(0);
    assertThrows(
        RoomNotAvailableOnDateException.class,
        () -> {
          LocalDateTime tomorrowNoon = LocalDateTime.of(now().plusDays(1), noon);
          log.info("Creating reservation at 16:00:01");
          Reservation reservationFourOclock =
              Reservation.builder()
                  .reservationStart(tomorrowNoon.plusHours(4).plusSeconds(1))
                  .reservationEnd(tomorrowNoon.plusHours(6))
                  .conferenceRoom(conferenceRoomOne)
                  .conferenceRoomUser(simpleUserTwo)
                  .status(ReservationStatus.PENDING)
                  .build();
          Reservation fromDB = reservationService.createReservation(reservationFourOclock);
          assertThat(fromDB).isNotNull();
          assertThat(fromDB.getConferenceRoom()).isNotNull();
          assertThat(fromDB.getConferenceRoomUser()).isNotNull();
          assertThat(reservationRepository.count()).isEqualTo(1L);

          log.info("Trying to create reservation again at 16:00:01");
          Reservation reservationFourOclockAgain =
              Reservation.builder()
                  .reservationStart(tomorrowNoon.plusHours(4).plusSeconds(1))
                  .reservationEnd(tomorrowNoon.plusHours(6))
                  .conferenceRoom(conferenceRoomOne)
                  .conferenceRoomUser(simpleUserTwo)
                  .status(ReservationStatus.PENDING)
                  .build();
          reservationService.createReservation(reservationFourOclockAgain);
          assertThat(reservationFourOclock).isNull();
          assertThat(reservationRepository.count()).isEqualTo(3L);

          Exception exception =
              assertThrows(
                  RoomNotAvailableOnDateException.class,
                  () -> reservationService.createReservation(reservationFourOclockAgain));
          String expectedMessage = "No vacant rooms available from";
          String actualMessage = exception.getMessage();
          assertTrue(actualMessage.contains(expectedMessage));
          assertThat(reservationFourOclockAgain).isNull();
          assertThat(reservationRepository.count()).isEqualTo(3L);
        },
        "Expected 'RoomNotAvailableOnDateException' to throw, but it didn't");
  }

  @Test
  @WithMockUser(
      username = "employee_1",
      roles = {"EMPLOYEE"})
  private void employeeWhenFetchAllReservations_ThenOk() throws Exception {
    final String RESERVATIONS_API_ROOT = "/api/v1/reservations";
    log.info("Fetching all reservations ...");
    this.mockMvc
        .perform(get(RESERVATIONS_API_ROOT + "/all"))
        .andExpect(authenticated().withRoles("EMPLOYEE"));
  }
}
