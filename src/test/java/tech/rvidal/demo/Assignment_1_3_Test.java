package tech.rvidal.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Rule;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.context.support.TestExecutionEvent;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import tech.rvidal.demo.utils.DtoConverter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class Assignment_1_3_Test extends RepositoryTestBase {

  @Rule
  public final JUnitRestDocumentation restDocumentation =
      new JUnitRestDocumentation("target/generated-snippets");

  @Autowired DtoConverter dtoConverter;

  @BeforeAll
  void setUp() throws Exception {
    cleanDatabase();
    log.info("Creating users and rooms ...");
    super.createEntities();
  }

  @BeforeEach
  void setup(
      WebApplicationContext webApplicationContext,
      RestDocumentationContextProvider restDocumentation) {
    super.setupBeforeEach(webApplicationContext, restDocumentation);
  }

  @AfterEach
  void clean() {
    cleanDatabase();
  }

  /**
   * ASSIGNMENT 1.3 : "As employee, I want to book an available room on my floor"
   *
   * <p>1) Through the API Book a room that is available, should succeed.Remove this room from the
   * DB.
   *
   * <p>2) Through the repository save a room to the DB, set its status to "TIMED_OUT" (simulating
   * that user has not confirmed the reservation). Book the that room again through the API for the
   * same date, it should succeed
   *
   * @throws Exception
   */
  @Test
  @WithUserDetails(
      value = "employee_1",
      setupBefore = TestExecutionEvent.TEST_EXECUTION,
      userDetailsServiceBeanName = "mockUserDetailsService")
  void whenFetchingInvoiceForConfirmedReservations_ThenOk() throws Exception {
    new HttpSessionCsrfTokenRepository();

    MvcResult result =
        this.mockMvc
            .perform(
                MockMvcRequestBuilders.get(
                        SERVER + "/api/v1/rooms/book/{roomNumber}/{beginDate}/{endDate}",
                        conferenceRoomOne.getLocation().getRoomNumber(),
                        firstDayNextMonth.plusHours(7).format(formatter),
                        firstDayNextMonth.plusHours(8).format(formatter))
                    .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(jsonPath("$.reservedBy").value("mockEmployee"))
            .andExpect(jsonPath("$.reservationStart").value("01-03-2021 07:00"))
            .andExpect(jsonPath("$.reservationEnd").value("01-03-2021 08:00"))
            .andExpect(jsonPath("$.status").value("PENDING"))
            .andExpect(jsonPath("$.location").value("Floor=1/Room=123A"))
            .andExpect(status().isOk())
            .andReturn();
    assertThat(reservationRepository.count()).isEqualTo(1L);

    /* remove the previous booked room from db */
    reservationRepository.deleteAllInBatch();
    assertThat(reservationRepository.count()).isEqualTo(0);

    /* create a timed-out reservation */
    bookRoom(
        firstDayNextMonth.plusHours(4),
        firstDayNextMonth.plusHours(6),
        conferenceRoomOne,
        simpleUserOne,
        "TIMED_OUT");
    assertThat(reservationRepository.count()).isEqualTo(1L);

    /* book that room again, should succeed (as it was not confirmed */
    this.mockMvc
        .perform(
            MockMvcRequestBuilders.get(
                    SERVER + "/api/v1/rooms/book/{roomNumber}/{beginDate}/{endDate}",
                    conferenceRoomOne.getLocation().getRoomNumber(),
                    firstDayNextMonth.plusHours(4).format(formatter),
                    firstDayNextMonth.plusHours(6).format(formatter))
                .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(jsonPath("$.reservedBy").value("mockEmployee"))
        .andExpect(jsonPath("$.reservationStart").value("01-03-2021 04:00"))
        .andExpect(jsonPath("$.reservationEnd").value("01-03-2021 06:00"))
        .andExpect(jsonPath("$.status").value("PENDING"))
        .andExpect(jsonPath("$.location").value("Floor=1/Room=123A"))
        .andExpect(status().isOk())
        .andReturn();
    assertThat(reservationRepository.count()).isEqualTo(2L);
  }
}
