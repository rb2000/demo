package tech.rvidal.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import tech.rvidal.demo.domain.ConferenceRoomUser;
import tech.rvidal.demo.domain.Location;
import tech.rvidal.demo.domain.UserRole;
import tech.rvidal.demo.repository.ConferenceRoomUserRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

@Slf4j
@Service
@Qualifier("mockUserDetailsService")
@Transactional
public class MockUserDetailsService implements UserDetailsService {

  private ConferenceRoomUserRepository userRepository;

  @Autowired
  public MockUserDetailsService(
      ConferenceRoomUserRepository userRepository, WebApplicationContext applicationContext) {
    this.userRepository = userRepository;
  }

  /**
   * create a mock user to be used with mockmvc. We need it as the book room method needs to extract
   * the current logged in user from the session, and the normal @WithMockUser annotation does not
   * provide that possibility
   *
   * @param username
   * @return
   */
  @Override
  public UserDetails loadUserByUsername(String username) {
    log.info("Authenticating  user with name '{}'", username);
    ConferenceRoomUser user =
        new ConferenceRoomUser(
            UUID.randomUUID(),
            "mockEmployee",
            "mockEmployee@test.com",
            // pwd is "somepassword" in plain text
            "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
            true,
            true,
            true,
            true,
            Location.builder().roomNumber("1A").build(),
            "teamOne",
            Collections.singleton(UserRole.EMPLOYEE),
            new ArrayList<>());
    userRepository.save(user);
    return user;
  }
}
