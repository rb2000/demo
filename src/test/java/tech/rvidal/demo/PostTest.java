package tech.rvidal.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Rule;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import tech.rvidal.demo.domain.Reservation;
import tech.rvidal.demo.domain.ReservationStatus;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class PostTest extends RepositoryTestBase {

  @Rule
  public final JUnitRestDocumentation restDocumentation =
      new JUnitRestDocumentation("target/generated-snippets");

  @BeforeAll
  void setUp() throws Exception {
    cleanDatabase();
    log.info("Creating users and rooms ...");
    super.createEntities();
  }

  @AfterEach
  void clean() {
    cleanDatabase();
  }

  @BeforeEach
  void setup(
      WebApplicationContext webApplicationContext,
      RestDocumentationContextProvider restDocumentation) {
    super.setupBeforeEach(webApplicationContext, restDocumentation);
  }

  /**
   * Tests the POST endpoint
   *
   * @throws Exception
   */
  @Test
  @WithMockUser(
      username = "employee_1",
      authorities = {"ADMIN", "APPLICATION"})
  void whenPostingReservation_ThenOk() throws Exception {
    new HttpSessionCsrfTokenRepository();

    log.info("Performing POST API call to endpoint '/api/v1/reservations/reservation'");
    Reservation re =
        Reservation.builder()
            .reservationStart(firstDayNextMonth.plusHours(4))
            .reservationEnd(firstDayNextMonth.plusHours(6))
            .conferenceRoom(conferenceRoomOne)
            .conferenceRoomUser(simpleUserOne)
            .status(ReservationStatus.valueOf("PENDING"))
            .build();
    String json = gson.toJson(re, Reservation.class);
    this.mockMvc
        .perform(
            MockMvcRequestBuilders.post(SERVER + "/api/v1/reservations/reservation")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isCreated());
  }
}
