package tech.rvidal.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Rule;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class Assignment_1_2_Test extends RepositoryTestBase {

  @Rule
  public final JUnitRestDocumentation restDocumentation =
      new JUnitRestDocumentation("target/generated-snippets");

  @BeforeAll
  void setUp() throws Exception {
    cleanDatabase();
    log.info("Creating users and rooms ...");
    super.createEntities();
  }

  @BeforeEach
  void setup(
      WebApplicationContext webApplicationContext,
      RestDocumentationContextProvider restDocumentation) {
    super.setupBeforeEach(webApplicationContext, restDocumentation);
  }

  /** ASSIGNMENT 1.2: "As employee, I want to book an available room on my floor." */
  @Order(1)
  @Test
  @WithMockUser(
      username = "employee_1",
      authorities = {"ADMIN", "APPLICATION"})
  void whenFetchingInvoiceForConfirmedReservations_ThenOk() throws Exception {
    new HttpSessionCsrfTokenRepository();
    log.info("Creating reservations ");
    /* create a reservation */
    bookRoom(
        firstDayNextMonth.plusHours(4),
        firstDayNextMonth.plusHours(6).plusMinutes(27),
        conferenceRoomOne,
        simpleUserOne,
        "PENDING");
    bookRoom(
        firstDayNextMonth.plusHours(7),
        firstDayNextMonth.plusHours(8),
        conferenceRoomOne,
        simpleUserTwo,
        "CONFIRMED");
    bookRoom(
        firstDayNextMonth.plusHours(9),
        firstDayNextMonth.plusHours(10),
        conferenceRoomOne,
        simpleUserThree,
        "CONFIRMED");
    bookRoom(
        firstDayNextMonth.plusHours(11),
        firstDayNextMonth.plusHours(12),
        conferenceRoomOne,
        simpleUserTwo,
        "PENDING");
    log.info("Created 4 reservations");
    log.info(
        "Performing API call to endpoint '/api/v1/rooms/availability/{flooNumber}/{beginDate}/{endDate}'");
    this.mockMvc
        .perform(
            MockMvcRequestBuilders.get(
                    SERVER + "/api/v1/rooms/availability/{flooNumber}/{beginDate}/{endDate}",
                    "1",
                    LocalDateTime.now().format(formatter),
                    firstDayNextMonth.plusHours(13).format(formatter))
                .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(jsonPath("$", hasSize(4)))
        .andExpect(jsonPath("$[0].reservedBy").value("employeey_1(from 'teamOne')"))
        .andExpect(jsonPath("$[0].reservationStart").value("01-03-2021 04:00"))
        .andExpect(jsonPath("$[0].status").value("PENDING"))
        .andExpect(jsonPath("$[1].reservedBy").value("employee_2(from 'teamOne')"))
        .andExpect(jsonPath("$[1].reservationStart").value("01-03-2021 07:00"))
        .andExpect(jsonPath("$[1].status").value("CONFIRMED"))
        .andExpect(status().isOk());
  }

  /**
   * tests the "/api/reservations/all" endpoint. This test a booking reservation that was creating
   * during the tests setup above.
   *
   * @throws Exception
   */
  @Order(2)
  @Test
  @WithMockUser(
      username = "employee_1",
      authorities = {"ADMIN", "APPLICATION"})
  void whenAuthorizedAndCallEndPoint_ThenOk() throws Exception {
    new HttpSessionCsrfTokenRepository();
    /* create a reservation */
    log.info("Creating reservation");
    bookRoom(
        firstDayNextMonth.plusHours(4),
        firstDayNextMonth.plusHours(6),
        conferenceRoomOne,
        simpleUserTwo,
        "PENDING");

    log.info("Performing GET API call to endpoint '/api/v1/reservations/all'");
    this.mockMvc
        .perform(
            MockMvcRequestBuilders.get(SERVER + RESERVATIONS_API_ENDPOINT + "/all")
                .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].status").value("PENDING"))
        .andExpect(jsonPath("$[0].location").value("Floor=1/Room=123A"))
        .andDo(
            resultHandler.document(
                responseFields(
                    fieldWithPath("[].reservationID").description("The reservation id"),
                    fieldWithPath("[].reservationStart")
                        .description("The beginning of the reservation"),
                    fieldWithPath("[].reservationEnd")
                        .description("The end of the reservation period"),
                    fieldWithPath("[].reservedBy")
                        .description("The name of the user who reserved the conference room"),
                    fieldWithPath("[].confirmationDate")
                        .description("The date on which the user confirmed the reservation"),
                    fieldWithPath("[].expireDate")
                        .description(
                            "The latest date a reservation must be confirmed, otherwise it will be marked for removal"),
                    fieldWithPath("[].totalNumberOfMinutes")
                        .description("The number of minutes of the reservation"),
                    fieldWithPath("[].totalFee")
                        .description("The fee that will be charged for the room booking"),
                    fieldWithPath("[].location").description("The location of the conference room"),
                    fieldWithPath("[].bookedOn").description("The date the room was booked"),
                    fieldWithPath("[].status")
                        .description(
                            "The reservation status (PENDING / EXPIRED/ CONFIRMED/ TIMEDOUT"),
                    fieldWithPath("[].currency").description("The currency,can be EURO,NOK,DOLLAR"),
                    fieldWithPath("[].feePerHour")
                        .description(
                            "The hourly fee for using "
                                + "the room. Fees are "
                                + "calculated in a minute basis"))));
  }
}
