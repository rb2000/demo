package tech.rvidal.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Rule;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import tech.rvidal.demo.utils.DtoConverter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class Assignment_1_4_Test extends RepositoryTestBase {

  @Rule
  public final JUnitRestDocumentation restDocumentation =
      new JUnitRestDocumentation("target/generated-snippets");

  @Autowired DtoConverter dtoConverter;

  @BeforeAll
  void setUp() throws Exception {
    cleanDatabase();
    log.info("Creating users and rooms ...");
    super.createEntities();
  }

  @BeforeEach
  void setup(
      WebApplicationContext webApplicationContext,
      RestDocumentationContextProvider restDocumentation) {
    super.setupBeforeEach(webApplicationContext, restDocumentation);
  }
  @AfterEach
  void clean() {
    cleanDatabase();
  }
  /**
   * ASSIGNMENT 1.4 :
   *
   * <p>"As an application, I want to release the unconfirmed reservations"
   *  Unconfirmed reservations have their statuses set to TIMED_OUT.
   *  When a booking is created, its status is set to PENDING. The user
   *  has a time to confirm it. Otherwise it will be suitable for removal
   *  When calling the "deleteAllUnconfirmed" endpoint ,the system checks all
   *  pending reservation if their time to live is over, setting their status
   *  to TIMED_OUT, then other method is called to remove all of them with that
   *  status.
   *
   * @throws Exception
   */
  @Test
  @WithMockUser(
      username = "employee_1",
      authorities = {"APPLICATION"})
  void whenRemovingUnconfirmedReservations_ThenOk() throws Exception {
    new HttpSessionCsrfTokenRepository();

    /* create confirmed and expired reservations */
    for (int i = 0; i < 10; i++) {
      bookRoom(
          firstDayNextMonth.plusHours(i),
          firstDayNextMonth.plusHours(i + 1).minusSeconds(1),
          conferenceRoomOne,
          simpleUserOne,
          i % 2 == 0 ? "CONFIRMED" : "EXPIRED");
    }
    assertThat(reservationRepository.count()).isEqualTo(10L);

    /* create attended and timed out reservations */
    for (int i = 10; i < 20; i++) { //
      bookRoom(
          firstDayNextMonth.plusHours(i),
          firstDayNextMonth.plusHours(i + 1).minusSeconds(1),
          conferenceRoomOne,
          simpleUserOne,
          i % 2 == 0 ? "ATTENDED" : "TIMED_OUT");
    }
    assertThat(reservationRepository.count()).isEqualTo(20L);

    /* as user with application role, remove all TIMED_OUT reservations */
    this.mockMvc
        .perform(
            MockMvcRequestBuilders.get(SERVER + "/api/v1/reservations/deleteAllUnconfirmed")
                .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andReturn();
    assertThat(reservationRepository.count()).isEqualTo(15L);
  }
}
