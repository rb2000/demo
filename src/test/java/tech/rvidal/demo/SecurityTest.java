package tech.rvidal.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.testng.Assert.assertTrue;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class SecurityTest extends RepositoryTestBase {

  @Rule
  public final JUnitRestDocumentation restDocumentation =
      new JUnitRestDocumentation("target/generated-snippets");

  @BeforeAll
  void setUp() throws Exception {
    roomRepository.deleteAll();
    userRepository.deleteAll();
    log.info("Creating users and rooms ...");
    super.createEntities();
  }

  @BeforeEach
  void setup(
      WebApplicationContext webApplicationContext,
      RestDocumentationContextProvider restDocumentation) {
    super.setup(webApplicationContext, restDocumentation);
  }

  /**
   * test simple user tries to access protected resource that needs APPLICATION role, which will
   * fail
   *
   * @throws AccessDeniedException
   */
  @Test
  @WithMockUser(
      username = "employee_1",
      authorities = {"EMPLOYEE"})
  void deleteAllWhenRoleEmployeeThenForbidden() throws Exception {
    log.info("Performing GET API call to endpoint '/api/v1/reservations/deleteAllUnconfirmed'");
    this.mockMvc
        .perform(get(SERVER + RESERVATIONS_API_ENDPOINT + "/deleteAllUnconfirmed"))
        .andExpect(
            result -> assertTrue(result.getResolvedException() instanceof AccessDeniedException))
        .andDo(print());
  }
}
