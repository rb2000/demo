package tech.rvidal.demo;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tech.rvidal.demo.domain.*;
import tech.rvidal.demo.repository.ConferenceRoomRepository;
import tech.rvidal.demo.repository.ConferenceRoomUserRepository;
import tech.rvidal.demo.repository.ReservationRepository;
import tech.rvidal.demo.service.ReservationService;
import tech.rvidal.demo.utils.DtoConverter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

/** Helpe class to create users and rooms. */
@Slf4j
class RepositoryTestBase {

  @Autowired protected ConferenceRoomUserRepository userRepository;
  @Autowired protected ConferenceRoomRepository roomRepository;
  @Autowired protected ReservationRepository reservationRepository;
  @Autowired Gson gson;
  ConferenceRoomUser simpleUserOne;
  ConferenceRoomUser simpleUserTwo;
  ConferenceRoomUser simpleUserThree;

  private ConferenceRoomUser adminUser;
  private ConferenceRoomUser applicationUser;

  ConferenceRoom conferenceRoomOne;
  ConferenceRoom conferenceRoomTwo;
  ConferenceRoom conferenceRoomThree;

  @Autowired protected ReservationService reservationService;
  @Autowired protected MockMvc mockMvc;
  @Autowired protected WebApplicationContext context;

  final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

  final String SERVER = "http://localhost:8080";
  final String RESERVATIONS_API_ENDPOINT = "/api/v1/reservations";

  /* get the first day of next month at noon ... we do this because
  we do not know before hand when this test will run, and need to book
  rooms in the future and until midnight
  */
  static LocalDateTime firstDayNextMonth =
      LocalDateTime.now().with(LocalTime.MIDNIGHT).with(TemporalAdjusters.firstDayOfNextMonth());
  RestDocumentationResultHandler resultHandler;
@Autowired DtoConverter dtoConverter;

  void setupBeforeEach(
      WebApplicationContext webApplicationContext,
      RestDocumentationContextProvider restDocumentation) {
    resultHandler =
        document(
            "{method-name}",
            preprocessRequest(prettyPrint()),
            preprocessResponse(prettyPrint(), removeMatchingHeaders("X.*", "Pragma", "Expires")));
    this.mockMvc =
        MockMvcBuilders.webAppContextSetup(context)
            .apply(springSecurity())
            .apply(documentationConfiguration(restDocumentation))
            .alwaysDo(resultHandler)
            .build();
    reservationRepository.deleteAllInBatch();
  }

  void setup(
      WebApplicationContext webApplicationContext,
      RestDocumentationContextProvider restDocumentation) {
    resultHandler =
        document(
            "{method-name}",
            preprocessRequest(prettyPrint()),
            preprocessResponse(prettyPrint(), removeMatchingHeaders("X.*", "Pragma", "Expires")));
    mockMvc =
        MockMvcBuilders.webAppContextSetup(context)
            .apply(springSecurity())
            .apply(documentationConfiguration(restDocumentation))
            .alwaysDo(resultHandler)
            .build();
    reservationRepository.deleteAllInBatch();
  }

  void createEntities() {
    Location fakeUserOneLocation = Location.builder().floor(1).roomNumber("12").build();
    Location fakeUserTwoLocation = Location.builder().floor(2).roomNumber("2B").build();
    Location conferenceRoomOneLocation = Location.builder().floor(1).roomNumber("123A").build();
    Location conferenceRoomTwoLocation = Location.builder().floor(2).roomNumber("201").build();
    Location conferenceRoomThreeLocation = Location.builder().floor(2).roomNumber("206").build();

    simpleUserOne =
        new ConferenceRoomUser(
            UUID.randomUUID(),
            "employeey_1",
            "employee1@test.com",
            "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s" + ".vTlsTO4a",
            true,
            true,
            true,
            true,
            fakeUserOneLocation,
            "teamOne",
            Collections.singleton(UserRole.EMPLOYEE),
            new ArrayList<>());
    simpleUserTwo =
        new ConferenceRoomUser(
            UUID.randomUUID(),
            "employee_2",
            "employee2@test.com",
            "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
            true,
            true,
            true,
            true,
            fakeUserTwoLocation,
            "teamOne",
            Collections.singleton(UserRole.EMPLOYEE),
            new ArrayList<>());

    simpleUserThree =
        new ConferenceRoomUser(
            UUID.randomUUID(),
            "employee_3",
            "employee3@test.com",
            "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
            true,
            true,
            true,
            true,
            fakeUserOneLocation,
            "teamTwo",
            Collections.singleton(UserRole.EMPLOYEE),
            new ArrayList<>());

    adminUser =
        new ConferenceRoomUser(
            UUID.randomUUID(),
            "admin",
            "admin@test.com",
            "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
            true,
            true,
            true,
            true,
            fakeUserOneLocation,
            "AdminTeam",
            Collections.singleton(UserRole.ADMIN),
            new ArrayList<>());
    applicationUser =
        new ConferenceRoomUser(
            UUID.randomUUID(),
            "application",
            "applicationUser@test.com",
            "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
            true,
            true,
            true,
            true,
            fakeUserOneLocation,
            "applicationTeam",
            Collections.singleton(UserRole.APPLICATION),
            new ArrayList<>());

    conferenceRoomOne =
        ConferenceRoom.builder()
            .hasVideoConferenceFeature(false)
            .roomFee(BigDecimal.valueOf(120))
            .roomFeeCurrency("EURO")
            .capacity(11)
            .location(conferenceRoomOneLocation)
            .build();
    conferenceRoomTwo =
        ConferenceRoom.builder()
            .hasVideoConferenceFeature(false)
            .roomFee(BigDecimal.valueOf(345.34))
            .roomFeeCurrency("EURO")
            .capacity(11)
            .location(conferenceRoomTwoLocation)
            .build();
    conferenceRoomThree =
        ConferenceRoom.builder()
            .hasVideoConferenceFeature(false)
            .roomFee(BigDecimal.valueOf(100.50))
            .roomFeeCurrency("EURO")
            .capacity(11)
            .location(conferenceRoomThreeLocation)
            .build();

    userRepository.save(simpleUserOne);
    userRepository.save(simpleUserTwo);
    userRepository.save(simpleUserThree);
    userRepository.save(adminUser);
    userRepository.save(applicationUser);
    assertThat(simpleUserOne).isNotNull();
    assertThat(userRepository.count()).isEqualTo(5L);
    conferenceRoomOne = roomRepository.save(conferenceRoomOne);
    conferenceRoomTwo = roomRepository.save(conferenceRoomTwo);
    conferenceRoomThree = roomRepository.save(conferenceRoomThree);
    assertThat(conferenceRoomOne).isNotNull();
    assertThat(conferenceRoomTwo).isNotNull();
    assertThat(conferenceRoomThree).isNotNull();
    assertThat(roomRepository.count()).isEqualTo(3L);
  }

  Reservation bookRoom(
      LocalDateTime start,
      LocalDateTime end,
      ConferenceRoom room,
      ConferenceRoomUser user,
      String status) {
    /* create a reservation */
    Reservation temp =
        Reservation.builder()
            .reservationStart(start)
            .reservationEnd(end)
            .conferenceRoom(room)
            .conferenceRoomUser(user)
            .status(ReservationStatus.valueOf(status))
            .build();
    /* save the reservation to database */
    Reservation saved = reservationService.createReservation(temp);
    Assertions.assertThat(saved).isNotNull();
    return saved;
  }

  void cleanDatabase(){
    reservationRepository.deleteAllInBatch();
    userRepository.deleteAllInBatch();
    roomRepository.deleteAllInBatch();
  }
}
