#!/bin/sh

echo "Starring application" 
exec java ${JAVA_OPTS} -noverify -XX:+AlwaysPreTouch -Djava.security.egd=file:/dev/./urandom -cp /app/resources/:/app/classes/:/app/libs/* "tech.rvidal.demo.DemoApplication"  "$@"
