package tech.rvidal.demo.exceptions;

public class RoomNotAvailableOnDateException extends RuntimeException {

  private static final long serialVersionUID = -3269406354167716855L;

  public RoomNotAvailableOnDateException(String message) {
    super(message);
  }
}
