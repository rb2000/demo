package tech.rvidal.demo.exceptions;

public class DoubleConfirmationException extends RuntimeException {
  private static final long serialVersionUID = 3325217445674794438L;

  public DoubleConfirmationException(String message) {
    super(message);
  }
}
