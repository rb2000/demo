package tech.rvidal.demo.exceptions;

public class ReservationTimedOutException extends RuntimeException {
  private static final long serialVersionUID = 3325217445674794438L;

  public ReservationTimedOutException(String message) {
    super(message);
  }
}
