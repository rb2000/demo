package tech.rvidal.demo.exceptions;

public class EmailAlreadyRegisteredException extends RuntimeException {

  private static final long serialVersionUID = -2221793070689393727L;

  public EmailAlreadyRegisteredException() {
    super("Email is already in use!");
  }
}
