package tech.rvidal.demo.exceptions;

import com.google.gson.JsonSyntaxException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;

/** Handler for exceptions Creates a response entity object */
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionHandlingAdvice {

  static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");


  @ResponseStatus(HttpStatus.BAD_REQUEST) // 400
  @ExceptionHandler(HttpMessageConversionException.class)
  @ResponseBody
  public ResponseEntity<?> handleBadMessageConversion(HttpMessageConversionException e, WebRequest request) {
    ExceptionDetails errorDetails =
            new ExceptionDetails(
                    getTimeNow(), e.getMessage(), "NOT_FOUND", request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST) // 400
  @ExceptionHandler(HttpMessageNotReadableException.class)
  @ResponseBody
  public ResponseEntity<?> handleHttpMessageNotReadable(HttpMessageNotReadableException e, WebRequest request) {
    ExceptionDetails errorDetails =            new ExceptionDetails(
                    getTimeNow(), e.getMessage(), "BAD_REQUEST", request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(DateTimeParseException.class)
  @ResponseBody
  public ResponseEntity<?> handleDateTimeParseException(DateTimeParseException e, WebRequest request) {
    ExceptionDetails errorDetails =            new ExceptionDetails(
                    getTimeNow(), e.getMessage(), "BAD_REQUEST", request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
  }
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(JsonSyntaxException.class)
  @ResponseBody
  public ResponseEntity<?> handleDateTJsonSyntaxException(JsonSyntaxException e, WebRequest request) {
    ExceptionDetails errorDetails =            new ExceptionDetails(
                    getTimeNow(), e.getMessage(), "BAD_REQUEST", request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<?> resourceNotFoundException(
      ResourceNotFoundException ex, WebRequest request) {
    ExceptionDetails errorDetails =
        new ExceptionDetails(
            getTimeNow(), ex.getMessage(), "NOT_FOUND", request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(OperationNotAuthorizedException.class)
  public ResponseEntity<?> resourceNotFoundException(
          OperationNotAuthorizedException ex, WebRequest request) {
    ExceptionDetails errorDetails =
            new ExceptionDetails(
                    getTimeNow(), ex.getMessage(), "User tried to access a " +
                    " protected resource",
                    request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(ReservationTimedOutException.class)
  public ResponseEntity<?> resourceNotFoundException(
      ReservationTimedOutException ex, WebRequest request) {
    ExceptionDetails errorDetails =
        new ExceptionDetails(
            getTimeNow(), ex.getMessage(), "Reservation time out", request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(RoomNotAvailableOnDateException.class)
  public ResponseEntity<ExceptionDetails> handle(
      RoomNotAvailableOnDateException ex, WebRequest request) {
    ExceptionDetails errorDetails =
        new ExceptionDetails(
            getTimeNow(),
            ex.getMessage(),
            "Room is not available on this date",
            request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(ResourceAlreadyExistsException.class)
  public ResponseEntity<ExceptionDetails> handle(
      ResourceAlreadyExistsException ex, WebRequest request) {
    return new ResponseEntity<>(getDetails(ex, request), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(DoubleConfirmationException.class)
  public ResponseEntity<ExceptionDetails> handle(
      DoubleConfirmationException ex, WebRequest request) {
    ExceptionDetails errorDetails =
        new ExceptionDetails(
            getTimeNow(),
            ex.getMessage(),
            "User already confirmed attendance for this room",
            request.getDescription(false));
    return new ResponseEntity<ExceptionDetails>(errorDetails, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(EmailAlreadyRegisteredException.class)
  public ResponseEntity<ExceptionDetails> handle(
      EmailAlreadyRegisteredException ex, WebRequest request) {
    ExceptionDetails errorDetails =
        new ExceptionDetails(
            getTimeNow(),
            ex.getMessage(),
            "This email is alredy registered",
            request.getDescription(false));
    return new ResponseEntity<ExceptionDetails>(errorDetails, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(RoomCapacityExceededException.class)
  public ResponseEntity<ExceptionDetails> handle(
      RoomCapacityExceededException ex, WebRequest request) {
    ExceptionDetails errorDetails =
        new ExceptionDetails(
            getTimeNow(), ex.getMessage(), "Room capacity exceeded", request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(UsernameAlreadyExistsException.class)
  public ResponseEntity<ExceptionDetails> handle(
      UsernameAlreadyExistsException ex, WebRequest request) {
    ExceptionDetails errorDetails =
        new ExceptionDetails(
            getTimeNow(),
            ex.getMessage(),
            "This user name is already taken.",
            request.getDescription(false));
    return new ResponseEntity<>(errorDetails, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(UserNotFoundException.class)
  public ResponseEntity<ExceptionDetails> handle(UserNotFoundException ex, WebRequest request) {
    return new ResponseEntity<>(getDetails(ex, request), HttpStatus.CONFLICT);
  }

  @ExceptionHandler({HttpUnauthorizedException.class})
  @ResponseBody
  @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
  public ResponseEntity<ExceptionDetails> handle(HttpUnauthorizedException ex, WebRequest request) {
    return new ResponseEntity<>(getDetails(ex, request), HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public void constraintViolationException(HttpServletResponse response) throws IOException {
    response.sendError(HttpStatus.BAD_REQUEST.value());
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<?> globleExcpetionHandler(Exception ex, WebRequest request) {
    return new ResponseEntity<>(getDetails(ex, request), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private ExceptionDetails getDetails(Exception ex, WebRequest request) {
    return new ExceptionDetails(
        getTimeNow(),
        ex.getMessage(),
        Arrays.toString(ex.getStackTrace()),
        request.getDescription(true));
  }

  public String getTimeNow() {
    return LocalDateTime.now().format(formatter);
  }
}
