package tech.rvidal.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class OperationNotAuthorizedException extends RuntimeException {

  private static final long serialVersionUID = 3424663044776944595L;

  public OperationNotAuthorizedException(String message) {
    super(message);
  }
}
