package tech.rvidal.demo.exceptions;

public class UserNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -5985222940202850963L;

  public UserNotFoundException(String message) {
    super(message);
  }
}
