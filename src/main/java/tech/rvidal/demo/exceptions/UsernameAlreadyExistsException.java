package tech.rvidal.demo.exceptions;

public class UsernameAlreadyExistsException extends RuntimeException {

  private static final long serialVersionUID = -8649831455533159382L;

  public UsernameAlreadyExistsException() {
    super("Login name already in use,please choose another one!");
  }
}
