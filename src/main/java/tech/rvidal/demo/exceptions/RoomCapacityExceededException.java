package tech.rvidal.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class RoomCapacityExceededException extends RuntimeException {

  private static final long serialVersionUID = 2002524446783002934L;

  public RoomCapacityExceededException(String message) {
    super(message);
  }
}
