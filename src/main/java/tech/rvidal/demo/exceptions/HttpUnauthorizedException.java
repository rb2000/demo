package tech.rvidal.demo.exceptions;

public class HttpUnauthorizedException extends RuntimeException {
  private static final long serialVersionUID = 3325217445674794438L;

  public HttpUnauthorizedException(String message) {
    super(message);
  }
}
