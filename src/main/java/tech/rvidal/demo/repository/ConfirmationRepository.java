package tech.rvidal.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tech.rvidal.demo.domain.Confirmation;

import java.util.UUID;

@Repository
@Transactional
public interface ConfirmationRepository extends JpaRepository<Confirmation, Long> {

  @Transactional(readOnly = true)
  @Query(
      "SELECT co FROM Confirmation co WHERE  co.userId=:userId AND "
          + "co.roomId=:roomId AND co.reservationId=:reservationId")
  Confirmation findByUserIdAndRoom(
      @Param("userId") UUID userId,
      @Param("roomId") long roomId,
      @Param("reservationId") long reservationId);

  @Modifying(clearAutomatically = true, flushAutomatically = true)
  @Query("UPDATE Confirmation co SET co.hasArrived=:true WHERE co.id=:confirmationID")
  void updateConfirmation(@Param("confirmationID") long confirmationID);
}
