package tech.rvidal.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import tech.rvidal.demo.domain.ConferenceRoomUser;
import tech.rvidal.demo.domain.Reservation;

import java.time.LocalDateTime;
import java.util.List;

@Transactional
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

  @Transactional(readOnly = true)
  @Query(
      "SELECT count(r) FROM Reservation r WHERE r.reservationStart "
          + "BETWEEN "
          + ":startDate  AND :endDate  OR r.reservationEnd BETWEEN "
          + "   :startDate AND :endDate ")
  int countReservationsWithinDateRange(
      @Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);

  @Transactional(readOnly = true)
  @Query(
          "FROM Reservation r WHERE (( :startDate BETWEEN "
                  + " r.reservationStart AND r.reservationEnd ) OR (:endDate " +
                  "BETWEEN r.reservationStart AND r.reservationEnd )) "
                  + " AND r.status<>'EXPIRED' "
                  + " AND r.status<>'TIMED_OUT' ")
  List<Reservation> findValidReservationsBlockingMyNewReservation(
          @Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);


  @Transactional(readOnly = true)
  @Query(
      "SELECT count(r) FROM Reservation r WHERE (r.reservationStart "
          + " BETWEEN "
          + ":startDate  AND :endDate ) OR (r.reservationEnd BETWEEN "
          + "   :startDate AND :endDate)  AND r.conferenceRoom.location.roomNumber=:roomNumber")
  int countReservationsWithinDateRangeAndRoomName(
      @Param("roomNumber") String roomNumber,
      @Param("startDate") LocalDateTime startDate,
      @Param("endDate") LocalDateTime endDate);

  @Transactional(readOnly = true)
  @Query(
      "FROM Reservation r WHERE r.reservationStart "
          + "<= :startDate  AND r.reservationEnd <= :endDate ")
  List<Reservation> findReservationsWithinDateRange(
      @Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);

  @Transactional(readOnly = true)
  @Query(
      "SELECT r FROM Reservation r,ConferenceRoom cr "
          + "WHERE r.conferenceRoom.location.floor=:floorNumber  AND"
          + "  :beginRange <= r.reservationStart AND  :endRange >=r.reservationStart"
       + " AND cr.conferenceRoomId= r.conferenceRoom.id")
  List<Reservation> findReservationsOnFloorByDate(
      @Param("floorNumber") int floorNumber,
      @Param("beginRange") LocalDateTime beginRange,
      @Param("endRange") LocalDateTime endRange);

  @Transactional(readOnly = true)
  @Query(
      "FROM Reservation r WHERE r.reservationStart > CURRENT_TIMESTAMP   "
          + "  AND r.conferenceRoom.location.floor =:floorNumber")
  List<Reservation> findReservationsFromNowOnForFloor(@Param("floorNumber") int floorNumber);

  @Transactional(readOnly = true)
  @Query(
      "SELECT  r FROM ConferenceRoom cr, Reservation r ,"
          + "ConferenceRoomUser u   "
          + "WHERE r MEMBER OF u.reservations AND r.conferenceRoom"
          + ".conferenceRoomId = cr.conferenceRoomId AND u=:user AND "
          + "     :beginRange <= r.reservationStart AND  :endRange >=r.reservationStart ") //
  List<Reservation> createFeeInvoiceForMonth(
      @Param("user") ConferenceRoomUser user,
      @Param("beginRange") LocalDateTime beginRange,
      @Param("endRange") LocalDateTime endRange);

  @Transactional(readOnly = true)
  @Query(
      "SELECT r FROM ConferenceRoom cr,Reservation r ,"
          + "  ConferenceRoomUser u "
          + "WHERE r MEMBER OF u.reservations AND r.conferenceRoom"
          + ".conferenceRoomId = cr.conferenceRoomId AND u.teamName=:teamName AND"
          + "     :beginRange <= r.reservationStart AND  :endRange >=r"
          + ".reservationStart AND r.status='CONFIRMED'")
  List<Reservation> createFeeInvoiceForMonthTeam(
      @Param("teamName") String teamName,
      @Param("beginRange") LocalDateTime beginRange,
      @Param("endRange") LocalDateTime endRange);

  @Transactional(readOnly = true)
  @Query("SELECT r FROM Reservation r " + "WHERE r.conferenceRoom.location.roomNumber=:roomNumber")
  Reservation findByConferenceRoomNumber(@Param("roomNumber") int roomNumber);

  @Modifying(clearAutomatically = true, flushAutomatically = true)
  @Query("DELETE FROM Reservation r WHERE r.status='TIMED_OUT'")
  void deleteUnconfirmedReservations();

  @Modifying(flushAutomatically = true)
  @Query("DELETE FROM Reservation r WHERE r.id=:id")
  void deleteSingleReservation(@Param("id") long id);

  /* set status  */
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  @Query("UPDATE Reservation r SET r.status='TIMED_OUT' WHERE r.id=:id")
  void setReservationStatusToTimedOut(@Param("id") long id);

  @Modifying(clearAutomatically = true, flushAutomatically = true)
  @Query("UPDATE Reservation r SET r.numberOfAttendants=:numberOfAttendants WHERE r.id=:id")
  void increaseNumberOfAttendants(
      @Param("id") long id, @Param("numberOfAttendants") int numberOfAttendants);

  /* confirm the reservation */
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  @Query("UPDATE Reservation r SET r.status='CONFIRMED' WHERE r.id=:id")
  void confirmReservation(@Param("id") long id);

  @Modifying(clearAutomatically = true, flushAutomatically = true)
  void deleteAllInBatch();
}
