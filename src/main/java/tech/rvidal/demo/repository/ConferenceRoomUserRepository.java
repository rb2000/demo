package tech.rvidal.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tech.rvidal.demo.domain.ConferenceRoomUser;

import java.util.Optional;
import java.util.UUID;

@Repository
@Transactional
public interface ConferenceRoomUserRepository extends JpaRepository<ConferenceRoomUser, UUID> {
  Optional<ConferenceRoomUser> findByEmail(String email);

  Optional<ConferenceRoomUser> findByUsernameIs(String userName);

  Optional<ConferenceRoomUser> findConferenceRoomUserByUserId(UUID id);

  @Modifying(clearAutomatically = true, flushAutomatically = true)
  void deleteAllInBatch();
}
