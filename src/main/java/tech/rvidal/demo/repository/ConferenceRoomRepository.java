package tech.rvidal.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import tech.rvidal.demo.domain.ConferenceRoom;
import tech.rvidal.demo.domain.Reservation;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Transactional
public interface ConferenceRoomRepository extends JpaRepository<ConferenceRoom, Long> {

  @Transactional(readOnly = true)
  @Query(
      "FROM ConferenceRoom cr "
          + "WHERE cr.location.floor=:floorNumber  AND"
          + " cr.hasVideoConferenceFeature=:hasVideoConferenceFeature"
          + " AND cr.hasDrawingBoard=:hasDrawingBoard ORDER BY  cr.location.floor")
  List<ConferenceRoom> getReservationByFloorBeamerAndDrawingBoard(
      @Param("floorNumber") int floorNumber,
      @Param("hasVideoConferenceFeature") boolean hasVideoConferenceFeature,
      @Param("hasDrawingBoard") boolean hasDrawingBoard);

  @Transactional(readOnly = true)
  @Query(
      "SELECT cr FROM ConferenceRoom cr,Reservation r "
          + "WHERE r.conferenceRoom.location.floor=:floorNumber  AND"
          + "  :beginRange <= r.reservationStart AND  :endRange >=r"
          + ".reservationStart  AND cr.conferenceRoomId= r.id")
  List<ConferenceRoom> checkAvailabilityOnFloor(
      @Param("floorNumber") int floorNumber,
      @Param("beginRange") LocalDateTime beginRange,
      @Param("endRange") LocalDateTime endRange);

  @Transactional(readOnly = true)
  @Query(
      "SELECT r FROM Reservation r "
          + "WHERE r.conferenceRoom.location.floor=:floorNumber  AND"
          + " :beginRange < beginRange ")
  List<Reservation> checkAvailabilityOnFloorFromDate(
      @Param("floorNumber") int floorNumber, @Param("beginRange") LocalDateTime beginRange);

  @Transactional(readOnly = true)
  @Query("SELECT cr FROM ConferenceRoom cr " + "WHERE cr.location.roomNumber=:roomNumber")
  ConferenceRoom findByConferenceRoomByRoomNumber(@Param("roomNumber") String roomNumber);

  @Transactional(readOnly = true)
  @Query("SELECT cr FROM ConferenceRoom cr " + " WHERE cr.location.floor=:floorNumber")
  List<ConferenceRoom> findByConferenceRoomByFloorNumber(@Param("floorNumber") int floorNumber);

  @Query("FROM ConferenceRoom cr ORDER BY cr.location.roomNumber")
  Optional<List<ConferenceRoom>> findAllSorted();

  @Transactional(readOnly = true)
  @Query("SELECT r FROM Reservation r " + " WHERE r.conferenceRoom.location.floor=:floorNumber")
  List<Reservation> getReservationsOnFloor(@Param("floorNumber") int floorNumber);

  @Modifying(clearAutomatically = true, flushAutomatically = true)
  void deleteAllInBatch();
}
