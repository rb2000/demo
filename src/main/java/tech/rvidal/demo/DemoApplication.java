package tech.rvidal.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Conference Room booking application demo. There are 3 security roles: ADMIN,EMPLOYEE and
 * APPLICATION An user with the application role can remove unconfirmed reservations from the
 * system.
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = "tech.rvidal.demo.repository")
public class DemoApplication {

  @Autowired
  public DemoApplication(){
  }

  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }
}

