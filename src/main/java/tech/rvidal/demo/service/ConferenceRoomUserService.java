package tech.rvidal.demo.service;

import tech.rvidal.demo.domain.ConferenceRoomUser;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

//@Service
public interface ConferenceRoomUserService {
  ConferenceRoomUser addConferenceUser(ConferenceRoomUser user);

  boolean isPasswordCorrect(Object password, UUID id);

  Optional<ConferenceRoomUser> getUserById(String id);

  Optional<ConferenceRoomUser> getUserByUserName(String userName);

  List<ConferenceRoomUser> findAll();

  void deleteAllInBatch();
}
