package tech.rvidal.demo.service;

import org.springframework.data.repository.query.Param;
import tech.rvidal.demo.domain.ConferenceRoom;
import tech.rvidal.demo.domain.Reservation;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/** Service Interface for managing {@link ConferenceRoom}. */
public interface ConferenceRoomService {

  /**
   * Save a conferenceRoom.
   *
   * @param conferenceRoom the entity to save.
   * @return the persisted entity.
   */
  ConferenceRoom save(ConferenceRoom conferenceRoom);

  /**
   * Get all the conferenceRooms.
   *
   * @return the list of entities.
   */
  List<ConferenceRoom> findAll();

  /**
   * Get the "id" conferenceRoom.
   *
   * @param id the id of the entity.
   * @return the entity.
   */
  Optional<ConferenceRoom> findOne(Long id);

  /**
   * Delete the "id" conferenceRoom.
   *
   * @param id the id of the entity.
   */
  void delete(Long id);

  /**
   * Fetches all reservations in a floor
   *
   * @param floorNumber
   * @param beginRange
   * @param endRange
   * @return
   */
  List<ConferenceRoom> checkAvailabilityOnFloor(
      int floorNumber, LocalDateTime beginRange, LocalDateTime endRange);

  List<Reservation> checkAvailabilityOnFloorFromNowOn(int floorNumber);

  List<ConferenceRoom> findByConferenceRoomByFloorNumber(@Param("floorNumber") int floorNumber);

  ConferenceRoom findByConferenceRoomByRoomNumber(String roomNumber);

  List<Reservation> getReservationsOnFloor(int floorNumber);

  List<ConferenceRoom> getReservationByFloorBeamerAndDrawingBoard(
      int floorNumber, boolean hasVideoConferenceFeature, boolean hasDrawingBoard);

  Optional<List<ConferenceRoom>> findAllSorted();

  void deleteAllInBatch();
}
