package tech.rvidal.demo.service;

import org.springframework.stereotype.Service;
import tech.rvidal.demo.domain.ConferenceRoomUser;
import tech.rvidal.demo.domain.Reservation;
import tech.rvidal.demo.domain.ReservationDTO;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public interface ReservationService {

  int countReservationsWithinDateRange(LocalDateTime startDate, LocalDateTime endDate);

  List<Reservation> findReservationsWithinDateRange(LocalDateTime startDate, LocalDateTime endDate);

  List<Reservation> findALl();

  Optional<Reservation> findReservationById(Long id);

  Reservation createReservation(Reservation reservation);

  Reservation updateReservation(long id, ReservationDTO reservationDTO);

  Reservation save(Reservation reservation);

  void deleteReservation(long id);

  void checkIfReservationsTimedOut();

  void deleteUnconfirmedReservations();

  boolean cancelReservation(long id) throws Exception;

  void setReservationStatusToTimedOut(long id);

  List<Reservation> createFeeInvoiceForMonthSingleUser(
          ConferenceRoomUser user, int month, int year);

  List<Reservation> createFeeInvoiceForMonthTeam(String teamName, int month, int year);

  void increaseNumberOfAttendants(long reservationId, int numberOfAttendants);

  void confirmReservation(long id);

  int countReservationsWithinDateRangeAndRoomName(
          String roomNumber, LocalDateTime startDate, LocalDateTime endDate);

  List<Reservation>  findReservationsOnFloorByDate(int floorNumber, LocalDateTime startDate, LocalDateTime endDate);

  void deleteAllInBatch();

}
