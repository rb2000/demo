package tech.rvidal.demo.service;

import tech.rvidal.demo.domain.Confirmation;

import java.util.UUID;

public interface ConfirmationService {
  Confirmation findByUserIdAndRoom(UUID userId, long roomId, long resevationId);

  Confirmation saveConfirmation(Confirmation confirmation);
}
