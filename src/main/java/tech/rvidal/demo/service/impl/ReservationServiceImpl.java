package tech.rvidal.demo.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.rvidal.demo.domain.ConferenceRoomUser;
import tech.rvidal.demo.domain.Reservation;
import tech.rvidal.demo.domain.ReservationDTO;
import tech.rvidal.demo.exceptions.OperationNotAuthorizedException;
import tech.rvidal.demo.exceptions.ReservationTimedOutException;
import tech.rvidal.demo.exceptions.RoomNotAvailableOnDateException;
import tech.rvidal.demo.exceptions.UserNotFoundException;
import tech.rvidal.demo.repository.ConferenceRoomUserRepository;
import tech.rvidal.demo.repository.ReservationRepository;
import tech.rvidal.demo.service.ReservationService;
import tech.rvidal.demo.utils.DtoConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;

@Slf4j
@Service
public class ReservationServiceImpl implements ReservationService {

  private final ReservationRepository reservationRepository;
  private final ConferenceRoomUserRepository userRepository;
  private final DtoConverter dtoConverter;
  private static final DateTimeFormatter formatter =
      DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

  @Autowired
  public ReservationServiceImpl(ReservationRepository reservationRepository,
                                ConferenceRoomUserRepository userRepository,
                                DtoConverter dtoConverter) {
    this.reservationRepository = reservationRepository;
    this.userRepository = userRepository;
    this.dtoConverter = dtoConverter;
  }

  @Override
  @Transactional(readOnly = true)
  public int countReservationsWithinDateRange(LocalDateTime startDate, LocalDateTime endDate) {
    return this.reservationRepository.countReservationsWithinDateRange(startDate, endDate);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Reservation> findReservationsWithinDateRange(
      LocalDateTime startDate, LocalDateTime endDate) {
    return reservationRepository.findReservationsWithinDateRange(startDate, endDate);
  }

  @Override
  public List<Reservation> findALl() {
    return reservationRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Reservation> findReservationById(Long id) {
    return reservationRepository.findById(id);
  }

  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public Reservation createReservation(Reservation reservation)
      throws RoomNotAvailableOnDateException {
    long reservationRangeInSecondds =
        reservation
            .getReservationStart()
            .until(reservation.getReservationEnd(), ChronoUnit.SECONDS);
    long timeToLive = reservationRangeInSecondds / 10;

    LocalDateTime endDate = reservation.getReservationEnd();
    LocalDateTime startDate = reservation.getReservationStart();
    LocalDateTime expireDate = startDate.plusSeconds(timeToLive);
    LocalDateTime now = LocalDateTime.now();
    checkArgument(startDate.isAfter(now), "Start date must be in the future");
    checkArgument(endDate.isAfter(now), "End date must be in the future");
    checkArgument(
        startDate.isEqual(endDate) || startDate.isBefore(reservation.getReservationEnd()),
        "End date must be equal to start date or greater than start date");
    checkArgument(
        startDate.until(endDate, ChronoUnit.HOURS) < 24, "Maximal reservation time is 24 hours");
    checkArgument(isSameDay(startDate, endDate), "You can only reserve a room until midnight");

    /* check if there is no overlapping with existing reservations that
     *  have PENDING or CONFIRMED statuses */
    if (reservationRepository
            .findValidReservationsBlockingMyNewReservation(startDate, endDate)
            .size() > 0) {
      String message =
          String.format("No vacant rooms available from " + "%s to %s", startDate, endDate);
      throw new RoomNotAvailableOnDateException(message);
    }
    reservation.setExpireDate(expireDate);
    Reservation fromDb = reservationRepository.save(reservation);
    log.info(
            "Created reservation with id {}, setting TTL to {}"
                    + " seconds, after that "
                    + "reservation status will be set to TIMED_OUT",
            reservation.getId(),
            timeToLive);
    return fromDb;
  }

  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public Reservation updateReservation(long id, ReservationDTO reservationDTO) {
    Reservation toBeUpdated = reservationRepository.getOne(id);
    ConferenceRoomUser user= userRepository.findByUsernameIs(reservationDTO.getConferenceRoomUser())
                       .orElseThrow(()->new UserNotFoundException("User not found"));
    toBeUpdated.setConferenceRoomUser(user);
    toBeUpdated=dtoConverter.updateFieldsToEntity(reservationDTO, toBeUpdated);
    return reservationRepository.save(toBeUpdated);
  }

  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public Reservation save(Reservation reservation) {
    return reservationRepository.save(reservation);
  }

  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public void deleteReservation(long id) {
    log.info("Deleting reservation with id {}", id);
    reservationRepository.deleteSingleReservation(id);
  }

  @Override
  public void checkIfReservationsTimedOut() {
    reservationRepository.findAll().stream()
        .forEach(
            r -> {
              long timeToLive =
                  r.getReservationStart().until(r.getReservationEnd(), ChronoUnit.SECONDS) / 10;
              if (r.getReservationStart().plusSeconds(timeToLive).isBefore(LocalDateTime.now())) {
                reservationRepository.setReservationStatusToTimedOut(r.getId());
                log.info("Setting reservation with id {} to timed out",
                         r.getId());
              }
            });
  }

  @Override
  public void deleteUnconfirmedReservations() {
    reservationRepository.deleteUnconfirmedReservations();
  }

  /* NOT IMPLEMENTED */
  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public boolean cancelReservation(long id) throws Exception {
    throw new Exception(" NOT IMPLEMENTED YET");
  }

  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public void setReservationStatusToTimedOut(long id) {
    reservationRepository.setReservationStatusToTimedOut(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Reservation> createFeeInvoiceForMonthSingleUser(
      ConferenceRoomUser user, int month, int year) {
    LocalDateTime temp = LocalDateTime.of(year, month, 1, 0, 0);
    LocalDateTime begin = createMonthBegin(temp);
    LocalDateTime end = createMonthEnd(temp);
    return reservationRepository.createFeeInvoiceForMonth(user, begin, end);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Reservation> createFeeInvoiceForMonthTeam(String teamName, int month, int year) {
    LocalDateTime temp = LocalDateTime.of(year, month, 1, 0, 0);
    LocalDateTime begin = createMonthBegin(temp);
    LocalDateTime end = createMonthEnd(temp);
    return reservationRepository.createFeeInvoiceForMonthTeam(teamName, begin, end);
  }

  @Override
  public void increaseNumberOfAttendants(long reservationId, int numberOfAttendants) {
    reservationRepository.increaseNumberOfAttendants(reservationId, numberOfAttendants);
  }

  @Override
  public void confirmReservation(long id) {
    reservationRepository
        .findById(id)
        .ifPresent(
            r -> {
              long timeToLive =
                  r.getReservationStart().until(r.getReservationEnd(), ChronoUnit.SECONDS) / 10;
              if (r.getReservationStart().plusSeconds(timeToLive).isAfter(LocalDateTime.now())) {
                Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                String loggedUser = ((ConferenceRoomUser) auth.getPrincipal()).getUsername();
                String userWhoBookedRoom = r.getConferenceRoomUser().getUsername();
                if (!userWhoBookedRoom.equals(loggedUser)) {
                  String message =
                      String.format(
                          "User  with name '%s' is not allowed to confirm "
                              + "reservation for "
                              + "the User with name '%s', only the user who "
                              + "booked a room can confirm the reservation.",
                          loggedUser, userWhoBookedRoom);
                  log.info(message);
                  throw new OperationNotAuthorizedException(message);
                }
                reservationRepository.confirmReservation(id);
              } else {
                reservationRepository.setReservationStatusToTimedOut(id);
                throw new ReservationTimedOutException(
                    "Reservation was not confirmed until "
                        + r.getReservationStart().plusSeconds(timeToLive)
                        + " so it was set to be removed from system.");
              }
            });
  }

  @Override
  public int countReservationsWithinDateRangeAndRoomName(
      String roomNumber, LocalDateTime startDate, LocalDateTime endDate) {
    return reservationRepository.countReservationsWithinDateRangeAndRoomName(
        roomNumber, startDate, endDate);
  }

  @Override
  public List<Reservation> findReservationsOnFloorByDate(
      int floorNumber, LocalDateTime startDate, LocalDateTime endDate) {
    return reservationRepository.findReservationsOnFloorByDate(floorNumber, startDate, endDate);
  }

  @Override
  public void deleteAllInBatch() {
    reservationRepository.deleteAllInBatch();
  }

  public static boolean isSameDay(LocalDateTime date1, LocalDateTime date2) {
    LocalDate localDate1 = LocalDate.of(date1.getYear(), date1.getMonth(), date1.getDayOfMonth());
    LocalDate localDate2 = LocalDate.of(date2.getYear(), date2.getMonth(), date2.getDayOfMonth());
    return localDate1.isEqual(localDate2);
  }

  public LocalDateTime createMonthBegin(LocalDateTime temp) {
    YearMonth ym = YearMonth.from(temp);
    LocalDate firstDay = ym.atDay(1);
    return LocalDateTime.of(firstDay, LocalTime.MIDNIGHT);
  }

  public LocalDateTime createMonthEnd(LocalDateTime temp) {
    YearMonth ym = YearMonth.from(temp);
    LocalDate last = ym.atEndOfMonth();
    return LocalDateTime.of(last, LocalTime.MIDNIGHT.minusSeconds(1));
  }
}
