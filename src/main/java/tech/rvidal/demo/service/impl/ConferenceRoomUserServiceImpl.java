package tech.rvidal.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.rvidal.demo.domain.ConferenceRoomUser;
import tech.rvidal.demo.repository.ConferenceRoomUserRepository;
import tech.rvidal.demo.service.ConferenceRoomUserService;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class ConferenceRoomUserServiceImpl implements
        ConferenceRoomUserService {

  private final ConferenceRoomUserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public ConferenceRoomUserServiceImpl(
      ConferenceRoomUserRepository userRepository, PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public ConferenceRoomUser addConferenceUser(ConferenceRoomUser user) {
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    return userRepository.save(user);
  }

  @Override
  @Transactional(readOnly = true)
  public boolean isPasswordCorrect(Object password, UUID id) {
    Optional<ConferenceRoomUser> user = userRepository.findById(id);
    if (user.isPresent()) {
      String stringPassword = (String) ((Map) password).get("password");
      return passwordEncoder.matches(stringPassword, user.get().getPassword());
    } else {
      throw new UsernameNotFoundException("Username not found");
    }
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ConferenceRoomUser> getUserById(String id) {
    return userRepository.findConferenceRoomUserByUserId(UUID.fromString(id));
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ConferenceRoomUser> getUserByUserName(String userName) {
    return userRepository.findByUsernameIs(userName);
  }

  @Override
  @Transactional(readOnly = true)
  public List<ConferenceRoomUser> findAll() {
    return userRepository.findAll();
  }

    @Override
    public void deleteAllInBatch() {
        userRepository.deleteAllInBatch();
    }
}
