package tech.rvidal.demo.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tech.rvidal.demo.service.ReservationService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/** Utility class to remove unconfirmed reservations Runs every day at midnight. */
@Slf4j
@Component
@Profile("scheduler")
@Configuration
@EnableAsync
@EnableScheduling
public class CleanUnconfirmedReservationsService {

  private ReservationService reservationService;

  public CleanUnconfirmedReservationsService(ReservationService reservationService) {
    this.reservationService = reservationService;
  }

  /**
   * Runs very night at 00:30 and set status to expired to reservations that were not confirmed in
   * 10% of the reservation time.
   */
  @Scheduled(cron = "0 0 0 * * ?")
  public synchronized void setExpireReservationsStatus() {
    log.info("CRON JOB: setting reservation status to TIMED_OUT on unconfirmed reservations");
    this.reservationService.findALl().stream()
                           .filter(
                                   r -> {
                                     long timeToLive =
                                             r.getReservationStart().until(r.getReservationEnd(), ChronoUnit.SECONDS) / 10;
                                     return (r.getBookedOn().plusSeconds(timeToLive).isBefore(LocalDateTime.now())
                                             && r.getStatus().toString().equals("PENDING"));
                                   })
                           .forEach(
                                   r -> {
                                     this.reservationService.setReservationStatusToTimedOut(r.getId());
                                   });
  }

  @Scheduled(cron = "0 30 0 * * ?")
  public synchronized void deleteReservations() {
    log.info("CRON JOB: Deleting all unconfirmed reservations");
    this.reservationService.deleteUnconfirmedReservations();
  }
}
