package tech.rvidal.demo.service.impl;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import tech.rvidal.demo.domain.Confirmation;
import tech.rvidal.demo.repository.ConfirmationRepository;
import tech.rvidal.demo.service.ConfirmationService;

import java.util.UUID;

// TODO FIXME  not full implement, not being used yet
@Service
public class ConfirmationServiceImpl implements ConfirmationService {

  final ConfirmationRepository confirmationRepository;

  public ConfirmationServiceImpl(ConfirmationRepository confirmationRepository) {
    this.confirmationRepository = confirmationRepository;
  }

  @Override
  public Confirmation findByUserIdAndRoom(UUID userId, long roomId, long reservationId) {
    return this.confirmationRepository.findByUserIdAndRoom(userId, roomId, reservationId);
  }

  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public Confirmation saveConfirmation(Confirmation confirmation) {
    return this.confirmationRepository.save(confirmation);
  }
}
