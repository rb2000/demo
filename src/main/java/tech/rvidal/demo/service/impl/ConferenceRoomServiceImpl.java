package tech.rvidal.demo.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.rvidal.demo.domain.ConferenceRoom;
import tech.rvidal.demo.domain.Reservation;
import tech.rvidal.demo.repository.ConferenceRoomRepository;
import tech.rvidal.demo.repository.ReservationRepository;
import tech.rvidal.demo.service.ConferenceRoomService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/** Service Implementation for managing {@link ConferenceRoom}. */
@Service
@Transactional
public class ConferenceRoomServiceImpl implements ConferenceRoomService {

  private final Logger log = LoggerFactory.getLogger(ConferenceRoomServiceImpl.class);

  private final ConferenceRoomRepository conferenceRoomRepository;
  private final ReservationRepository reservationRepository;

  public ConferenceRoomServiceImpl(
      ConferenceRoomRepository conferenceRoomRepository,
      ReservationRepository reservationRepository) {
    this.conferenceRoomRepository = conferenceRoomRepository;
    this.reservationRepository = reservationRepository;
  }

  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public ConferenceRoom save(ConferenceRoom conferenceRoom) {
    log.debug("Request to save ConferenceRoom : {}", conferenceRoom);
    return conferenceRoomRepository.save(conferenceRoom);
  }

  @Override
  @Transactional(readOnly = true)
  public List<ConferenceRoom> findAll() {
    log.debug("Request to get all ConferenceRooms");
    return conferenceRoomRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ConferenceRoom> findOne(Long id) {
    log.debug("Request to get ConferenceRoom : {}", id);
    return conferenceRoomRepository.findById(id);
  }

  @Override
  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public void delete(Long id) {
    log.debug("Request to delete ConferenceRoom : {}", id);
    conferenceRoomRepository.deleteById(id);
  }

  /**
   * Fetches all reservations in a floor
   *
   * @param floorNumber
   * @param beginRange
   * @param endRange
   * @return
   */
  @Override
  public List<ConferenceRoom> checkAvailabilityOnFloor(
      int floorNumber, LocalDateTime beginRange, LocalDateTime endRange) {
    return conferenceRoomRepository.checkAvailabilityOnFloor(floorNumber, beginRange, endRange);
  }

  @Override
  public List<Reservation> checkAvailabilityOnFloorFromNowOn(int floorNumber) {
    return reservationRepository.findReservationsFromNowOnForFloor(floorNumber);
  }

  @Override
  public List<ConferenceRoom> findByConferenceRoomByFloorNumber(int floorNumber) {
    return conferenceRoomRepository.findByConferenceRoomByFloorNumber(floorNumber);
  }

  @Override
  public ConferenceRoom findByConferenceRoomByRoomNumber(String roomNumber) {
    return conferenceRoomRepository.findByConferenceRoomByRoomNumber(roomNumber);
  }

  @Override
  public List<Reservation> getReservationsOnFloor(int floorNumber) {
    return conferenceRoomRepository.getReservationsOnFloor(floorNumber);
  }

  @Override
  public List<ConferenceRoom> getReservationByFloorBeamerAndDrawingBoard(int floorNumber, boolean hasVideoConferenceFeature, boolean hasDrawingBoard) {
    return conferenceRoomRepository.getReservationByFloorBeamerAndDrawingBoard(floorNumber,hasVideoConferenceFeature,hasDrawingBoard);
  }

  @Override
  public Optional<List<ConferenceRoom>> findAllSorted() {
    return conferenceRoomRepository.findAllSorted();
  }

  @Override
  public void deleteAllInBatch() {
    conferenceRoomRepository.deleteAllInBatch();
  }


}
