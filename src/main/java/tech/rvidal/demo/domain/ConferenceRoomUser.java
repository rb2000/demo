package tech.rvidal.demo.domain;

import com.fasterxml.jackson.annotation.*;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity
@Table(name = "CONFERENCE_ROOM_USER")
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "userId")
public class ConferenceRoomUser implements UserDetails {

  @Id
  @Column(name = "user_id", updatable = false, nullable = false, unique = true)
  private UUID userId;

  @Column(name = "user_namme", nullable = false, unique = true)
  private String username;

  private String email;
  private String password;
  private boolean accountNonExpired;
  private boolean credentialsNonExpired;
  private boolean accountNonLocked;
  private boolean enabled;

  @Embedded private Location worksOnLocation;

  @Column(name = "team_name")
  private String teamName;

  @Column(name = "role")
  @OnDelete(action = OnDeleteAction.CASCADE)
  @ElementCollection(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  @Enumerated(EnumType.STRING)
  @NotNull
  @OrderColumn
  private Set<UserRole> roles;

  public ConferenceRoomUser(
      UUID id,
      String username,
      String email,
      String password,
      boolean accountNonExpired,
      boolean accountNonLocked,
      boolean credentialsNonExpired,
      boolean enabled,
      Location worksOnLocation,
      String teamName,
      @NotNull Set<UserRole> roles,
      List<Reservation> reservation) {

    this.userId = id;
    this.username = username;
    this.email = email;
    this.password = password;
    this.worksOnLocation = worksOnLocation;
    this.teamName = teamName;
    this.roles = roles;
    this.accountNonExpired = true;
    this.accountNonLocked = true;
    this.credentialsNonExpired = true;
    this.enabled = true;
  }

  @JsonIgnore
  @OrderBy("reservationStart ASC")
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "conferenceRoomUser", cascade = CascadeType.REMOVE)
  List<Reservation> reservations = new ArrayList<>();

  @Override
  public List<GrantedAuthority> getAuthorities() {
    List<GrantedAuthority> authorities = new ArrayList<>();
    roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.toString())));
    return authorities;
  }

  public void addRole(UserRole userRole) {
    this.roles.add(userRole);
  }

  public void removeRoles() {
    this.setRoles(null);
  }
}
