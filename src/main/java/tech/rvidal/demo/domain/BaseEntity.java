package tech.rvidal.demo.domain;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;


/**
 * Base class for DTOs. Overrides toString method to pretty print json format
 */
@Slf4j
@EqualsAndHashCode
public class BaseEntity {


    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";
        try {
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            jsonString = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            log.error(e.toString());
        }
        return jsonString;
    }

}
