package tech.rvidal.demo.domain;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class InvoiceVO extends BaseEntity {
    private String invoiceFor;
    private List<ReservationVO> reservations;
    private BigDecimal sum;
    private double totalTimeInMinutes;
    private String currency;

    public void addReservation(ReservationVO reservationVO){
        this.reservations.add(reservationVO);
    }
}
