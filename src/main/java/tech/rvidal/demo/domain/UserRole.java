package tech.rvidal.demo.domain;

public enum UserRole {
    EMPLOYEE,
    ADMIN,
    APPLICATION,
    GUEST

}
