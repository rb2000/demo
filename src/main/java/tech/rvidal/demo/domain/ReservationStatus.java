package tech.rvidal.demo.domain;

public enum ReservationStatus {
    PENDING,
    CONFIRMED,
    EXPIRED,
    ATTENDED,
    TIMED_OUT
}
