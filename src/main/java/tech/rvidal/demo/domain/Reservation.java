package tech.rvidal.demo.domain;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@AllArgsConstructor
@NoArgsConstructor
@Table(name = "RESERVATION")
@Entity
@Data
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@ToString(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reservation implements Serializable {

  private static final long serialVersionUID = -5831382104152540652L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "reservation_id", updatable = false, nullable = false)
  private Long id;

  @NotNull
  @FutureOrPresent(message = "Reservation start date must be now or in the future")
  @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm",iso = DateTimeFormat.ISO.DATE_TIME)
  private LocalDateTime reservationStart;

  @NotNull
  @Future(message = "Reservation end date must be now or in the future")
  @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm",iso = DateTimeFormat.ISO.DATE_TIME)
  private LocalDateTime reservationEnd;

  @Future(message = "Reservation end date must be now or in the future")
  @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm",iso = DateTimeFormat.ISO.DATE_TIME)
  private LocalDateTime confirmationDate;

  @Future(message = "Reservation end date must be now or in the future")
  @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm",iso = DateTimeFormat.ISO.DATE_TIME)   @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
  private LocalDateTime expireDate;


  @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm",iso = DateTimeFormat.ISO.DATE_TIME)   @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
  private LocalDateTime bookedOn;

  @PrePersist
  void setBookingDate(){
    this.bookedOn=LocalDateTime.now();
  }

  @OneToOne
  @JoinColumn(name = "user_id")
  private ConferenceRoomUser conferenceRoomUser;

  @ManyToOne
  @JoinColumn(name = "conference_room_id")
  private ConferenceRoom conferenceRoom;

  @Enumerated(EnumType.STRING)
  @NotNull
  private ReservationStatus status;

  @Column(name = "total_fee")
  private double totalFeeForTime;

  @Column(name = "number_of_attendants")
  private int numberOfAttendants = 0;
}
