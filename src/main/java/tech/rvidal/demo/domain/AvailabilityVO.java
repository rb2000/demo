package tech.rvidal.demo.domain;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AvailabilityVO extends BaseEntity {
    private String reservationID;
    private String roomNumber;
    private String roomFloor;
    private String reservedBy;
    private String reservationStart;
    private String reservationEnd;
    private String confirmationDate;
    private String expireDate;
    private String status;

}
