package tech.rvidal.demo.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

// FIXME : class is not being used. Make it use entities, and avoid double reservation confirmations

@AllArgsConstructor
@NoArgsConstructor
@Table(name="confirmation")
@Entity
@Data
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Confirmation  extends  BaseEntity implements Serializable {

    private static final long serialVersionUID = -6393303275535399446L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="confirmation_id", updatable = false, nullable = false)
    private Long id;
    @Column(name="has_arrived", updatable = false, nullable = false)
    private boolean hasArrived;
    @Column(name="reservation_id", updatable = false, nullable = false)
    long reservationId;
    @Column(name="user_id", nullable = false)
    UUID userId;
    @Column(name="room_id", nullable = false)
    long roomId;
    @Column(name="arrival_date", updatable = false, nullable = false)
    private LocalDateTime arrivalDate;
}
