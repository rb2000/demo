package tech.rvidal.demo.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * View class to show a smaller set of information about
 * the reservation. Used in serialization/api requests
 */
@AllArgsConstructor
@Data
@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ReservationVO extends BaseEntity {
    private String reservationID;
    private String reservedBy;
    private String reservationStart;
    private String reservationEnd;
    private String confirmationDate;
    private String expireDate;
    private String bookedOn;
    private String location;
    private String status;
    private String totalFee;
    private String currency;
    private String feePerHour;
    private String totalNumberOfMinutes;

    public ReservationVO() {
    super();
    }
}
