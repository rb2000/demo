package tech.rvidal.demo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class ReservationDTO {

 private String reservationStart;
  private String reservationEnd;
  private String confirmationDate;
  private String expireDate;
  private String bookedOn;
  private String conferenceRoomUser;
  private String conferenceRoom;
  private String status;
  private double totalFeeForTime;
}
