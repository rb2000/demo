package tech.rvidal.demo.domain;

public enum Currency {
    DOLLAR,
    EURO,
    NOK
}
