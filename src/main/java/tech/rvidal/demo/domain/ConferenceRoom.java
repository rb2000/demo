package tech.rvidal.demo.domain;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CONFERENCE_ROOM")
@Entity
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "conferenceRoomId")
@ToString(callSuper = true)
public class ConferenceRoom implements Serializable {

  private static final long serialVersionUID = -1911437700908133775L;
  @JsonIgnore
  @OneToMany(
      fetch = FetchType.EAGER,
      cascade = CascadeType.ALL,
      targetEntity = Reservation.class,
      mappedBy = "conferenceRoom")
  List<Reservation> reservations = new ArrayList<>();
  @Id
  @Column(name = "conference_room_id", updatable = false, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long conferenceRoomId;
  @Column(name = "has_drawing_board")
  private boolean hasDrawingBoard;
  @Column(name = "has_video_conference")
  private boolean hasVideoConferenceFeature;
  @Embedded private Location location;
  private int capacity;
  @Column(name = "number_of_current_users")
  private int numberOfCurrentUsers;
  @NotNull
  @Column(name = "room_fee", precision = 5, scale = 2)
  private BigDecimal roomFee;
  @NotNull
  @Column(name = "currency")
  private String roomFeeCurrency;

  public ConferenceRoom addReservation(Reservation reservation) {
    this.reservations.add(reservation);
    reservation.setConferenceRoom(this);
    return this;
  }

  public ConferenceRoom removeReservation(Reservation reservation) {
    this.reservations.remove(reservation);
    reservation.setConferenceRoom(null);
    return this;
  }
}
