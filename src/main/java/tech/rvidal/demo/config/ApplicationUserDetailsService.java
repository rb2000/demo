package tech.rvidal.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import tech.rvidal.demo.domain.ConferenceRoomUser;
import tech.rvidal.demo.repository.ConferenceRoomUserRepository;

import java.util.Optional;

@Slf4j
@Service
@Qualifier("applicationUserDetailsService")
@Transactional
public class ApplicationUserDetailsService implements UserDetailsService {

  private ConferenceRoomUserRepository userRepository;

  @Autowired
  public ApplicationUserDetailsService(
      ConferenceRoomUserRepository userRepository, WebApplicationContext applicationContext) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) {
    log.info("Authenticating  user with name '{}'", username);
    Optional<ConferenceRoomUser> user = userRepository.findByUsernameIs(username);
    if (user.isPresent()) {
      log.info("Password:'{}' Role:'{}' ", user.get().getPassword(), user.get().getRoles());

      return user.get();
    } else {
      throw new UsernameNotFoundException(("Username not found"));
    }
  }
}
