package tech.rvidal.demo.utils;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.rvidal.demo.domain.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class DtoConverter {

  private final Gson gson;

  private static final DateTimeFormatter formatter =
      DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

  public DtoConverter(Gson gson) {
    this.gson = gson;
  }

  public ReservationVO convertToReservationsView(Reservation reservation) {
    ReservationVO r = new ReservationVO();
    if (null != reservation.getId()) {
      r.setReservationID(reservation.getId().toString());
    }
    r.setReservationStart(reservation.getReservationStart().format(formatter));
    r.setReservationEnd(reservation.getReservationEnd().format(formatter));
    r.setReservedBy(reservation.getConferenceRoomUser().getUsername());
    r.setConfirmationDate(
        reservation.getConfirmationDate() != null
            ? (reservation.getConfirmationDate().format(formatter))
            : "");
    r.setBookedOn(
        reservation.getBookedOn() != null ? (reservation.getBookedOn().format(formatter)) : "");
    try {
      r.setFeePerHour(String.valueOf(reservation.getConferenceRoom().getRoomFee()));
    } catch (Exception e) {
      e.printStackTrace();
    }
    r.setCurrency(String.valueOf(reservation.getConferenceRoom().getRoomFeeCurrency()));
    r.setStatus(reservation.getStatus().toString());
    if (null != reservation.getExpireDate()) {
      r.setExpireDate(reservation.getExpireDate().format(formatter));
    }
    r.setLocation(
        "Floor="
            + reservation.getConferenceRoom().getLocation().getFloor()
            + "/Room="
            + reservation.getConferenceRoom().getLocation().getRoomNumber());
    return r;
  }

  public ReservationDTO convertToDto(Reservation reservation) {
    ReservationDTO r = new ReservationDTO();
    r.setReservationStart(reservation.getReservationStart().format(formatter));
    r.setReservationEnd(reservation.getReservationEnd().format(formatter));
    r.setConferenceRoom(reservation.getConferenceRoom().toString());
    r.setConferenceRoomUser(reservation.getConferenceRoomUser().getUsername());
    r.setConfirmationDate(reservation.getConfirmationDate().format(formatter));
    r.setStatus(reservation.getStatus().toString());
    r.setTotalFeeForTime(reservation.getTotalFeeForTime());
    r.setExpireDate(reservation.getExpireDate().format(formatter));
    r.setBookedOn(reservation.getBookedOn().format(formatter));
    return r;
  }

  public Reservation updateFieldsToEntity(ReservationDTO dto, Reservation toBeUpdated) {
    ConferenceRoom room=  gson.fromJson(dto.getConferenceRoom(), ConferenceRoom.class);
    toBeUpdated.setReservationStart(LocalDateTime.parse(dto.getReservationStart(), formatter));
    toBeUpdated.setReservationEnd(LocalDateTime.parse(dto.getReservationEnd(), formatter));
    toBeUpdated.setConferenceRoom(room);
    toBeUpdated.setConfirmationDate(LocalDateTime.parse(dto.getConfirmationDate(), formatter));
    toBeUpdated.setStatus(ReservationStatus.valueOf(dto.getStatus()));
    toBeUpdated.setTotalFeeForTime(dto.getTotalFeeForTime());
    toBeUpdated.setExpireDate(LocalDateTime.parse(dto.getExpireDate(), formatter));
    toBeUpdated.setBookedOn(LocalDateTime.parse(dto.getBookedOn(), formatter));
    return toBeUpdated;
  }
}
