package tech.rvidal.demo.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import tech.rvidal.demo.domain.*;
import tech.rvidal.demo.repository.ConferenceRoomRepository;
import tech.rvidal.demo.repository.ConferenceRoomUserRepository;
import tech.rvidal.demo.repository.ReservationRepository;
import tech.rvidal.demo.service.ConferenceRoomService;
import tech.rvidal.demo.service.ReservationService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.UUID;

import static java.time.LocalDate.now;

/** Helper class , creates some uers with different privileges(ADMIN, EMPLOYEE,APPLICATION) */
@Slf4j
@Component
public class DataInitializer implements ApplicationListener<ApplicationReadyEvent> {

  private static RestTemplate restTemplate = new RestTemplate();
  final ConferenceRoomUserRepository userRepository;
  final ConferenceRoomRepository roomRepository;
  final ReservationRepository reservationRepository;
  final ReservationService reservationService;
  final ConferenceRoomService roomService;

  public DataInitializer(
      ConferenceRoomUserRepository userRepository,
      ConferenceRoomRepository roomRepository,
      ReservationRepository reservationRepository,
      ReservationService reservationService,
      ConferenceRoomService roomService) {
    this.userRepository = userRepository;
    this.roomRepository = roomRepository;
    this.reservationRepository = reservationRepository;
    this.reservationService = reservationService;
    this.roomService = roomService;
  }

  /*
  all passwords are "somepassword" in plain text, here encrypted with bcrypt
   */
  @Override
  public void onApplicationEvent(final ApplicationReadyEvent event) {
    Location fakeUserOneLocation;
    Location fakeUserTwoLocation;
    Location conferenceRoomOneLocation;
    Location conferenceRoomTwoLocation;
    Location conferenceRoomThreeLocation;

    log.info("Creating users and conference rooms");
    fakeUserOneLocation = Location.builder().floor(1).roomNumber("12").build();
    fakeUserTwoLocation = Location.builder().floor(2).roomNumber("2B").build();
    conferenceRoomOneLocation = Location.builder().floor(1).roomNumber("123A").build();
    conferenceRoomTwoLocation = Location.builder().floor(2).roomNumber("201").build();
    conferenceRoomThreeLocation = Location.builder().floor(3).roomNumber("306").build();

    ConferenceRoomUser employee1 =
        userRepository.save(
            new ConferenceRoomUser(
                UUID.randomUUID(),
                "employee1",
                "employee1@test.com",
                //   "{bcrypt}somepassword",
                "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
                true,
                true,
                true,
                true,
                fakeUserOneLocation,
                "teamOne",
                Collections.singleton(UserRole.EMPLOYEE),
                null));

    ConferenceRoomUser employee2 =
        userRepository.save(
            new ConferenceRoomUser(
                UUID.randomUUID(),
                "employee2",
                "employee2@test.com",
                "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
                true,
                true,
                true,
                true,
                fakeUserOneLocation,
                "teamOne",
                Collections.singleton(UserRole.EMPLOYEE),
                null));

    ConferenceRoomUser employee3 =
        userRepository.save(
            new ConferenceRoomUser(
                UUID.randomUUID(),
                "employee3",
                "employee3@test.com",
                "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
                true,
                true,
                true,
                true,
                fakeUserTwoLocation,
                "teamTwo",
                Collections.singleton(UserRole.EMPLOYEE),
                null));

    userRepository.save(
        new ConferenceRoomUser(
            UUID.randomUUID(),
            "admin",
            "admin@test.com",
            "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
            true,
            true,
            true,
            true,
            fakeUserOneLocation,
            "AdminTeam",
            Collections.singleton(UserRole.ADMIN),
            null));

    userRepository.save(
        new ConferenceRoomUser(
            UUID.randomUUID(),
            "application",
            "applicationUser@test.com",
            "$2y$12$FaumSSFpMl15z0X1IUWKo.aTafcD3BoDlrqTdZ/EFd2s.vTlsTO4a",
            true,
            true,
            true,
            true,
            fakeUserOneLocation,
            "applicationTeam",
            new HashSet<>(Arrays.asList(UserRole.APPLICATION, UserRole.ADMIN)),
            null));

    ConferenceRoom conferenceRoomOne =
        roomRepository.save(
            ConferenceRoom.builder()
                .hasVideoConferenceFeature(false)
                .roomFee(BigDecimal.valueOf(120))
                .capacity(11)
                .roomFeeCurrency("EURO")
                .location(conferenceRoomOneLocation)
                .build());

    ConferenceRoom conferenceRoonTwo =
        roomRepository.save(
            ConferenceRoom.builder()
                .hasVideoConferenceFeature(false)
                .roomFee(BigDecimal.valueOf(345.34))
                .capacity(17)
                .roomFeeCurrency("EURO")
                .location(conferenceRoomTwoLocation)
                .build());

    ConferenceRoom conferenceRoonThree =
        roomRepository.save(
            ConferenceRoom.builder()
                .hasVideoConferenceFeature(false)
                .roomFee(BigDecimal.valueOf(50))
                .capacity(3)
                .roomFeeCurrency("EURO")
                .location(conferenceRoomThreeLocation)
                .build());

    LocalDateTime tomorrowNoon = LocalDateTime.of(now().plusDays(1), LocalTime.NOON);

    Reservation reservationNoon =
        Reservation.builder()
            .reservationStart(tomorrowNoon.minusHours(8))
            .reservationEnd(tomorrowNoon.minusHours(7))
            .conferenceRoom(conferenceRoomOne)
            .conferenceRoomUser(employee1)
            .status(ReservationStatus.PENDING)
            .build();
    Reservation reservationTemp =
        Reservation.builder()
            .reservationStart(tomorrowNoon.minusHours(12))
            .reservationEnd(tomorrowNoon.minusHours(11))
            .conferenceRoom(conferenceRoonThree)
            .conferenceRoomUser(employee1)
            .status(ReservationStatus.PENDING)
            .build();

    Reservation reservationNoon2 =
        Reservation.builder()
            .reservationStart(tomorrowNoon.minusHours(10))
            .reservationEnd(tomorrowNoon.minusHours(9))
            .conferenceRoom(conferenceRoonTwo)
            .conferenceRoomUser(employee1)
            .status(ReservationStatus.PENDING)
            .build();

    Reservation reservationNoon3 =
        Reservation.builder()
            .reservationStart(tomorrowNoon.minusHours(6))
            .reservationEnd(tomorrowNoon.minusHours(5))
            .conferenceRoom(conferenceRoonTwo)
            .conferenceRoomUser(employee3)
            .status(ReservationStatus.PENDING)
            .build();

    reservationService.createReservation(reservationTemp);
    reservationService.createReservation(reservationNoon);
    reservationService.createReservation(reservationNoon2);
    reservationService.createReservation(reservationNoon3);
  }
}
