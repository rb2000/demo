package tech.rvidal.demo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.rvidal.demo.domain.ConferenceRoomUser;
import tech.rvidal.demo.domain.InvoiceVO;
import tech.rvidal.demo.domain.Reservation;
import tech.rvidal.demo.domain.ReservationVO;
import tech.rvidal.demo.exceptions.UserNotFoundException;
import tech.rvidal.demo.repository.ConferenceRoomUserRepository;
import tech.rvidal.demo.service.ReservationService;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Validated
@RequestMapping("/api/v1/invoices")
public class InvoiceController {

  private final ReservationService reservationService;
  private final ConferenceRoomUserRepository userRepository;

  public InvoiceController(
      ReservationService reservationService, ConferenceRoomUserRepository userRepository) {
    this.reservationService = reservationService;
    this.userRepository = userRepository;
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @GetMapping("/singleuser/{userName}/{month}/{year}")
  ResponseEntity<InvoiceVO> getFeesForUserAndMonth(
      @PathVariable String userName,
      @PathVariable
          @Min(value = 1, message = "day must be greater than or equal to 1")
          @Max(value = 31, message = "day must be lower than 31")
          int month,
      @PathVariable
          @Min(value = 2020, message = "year must be greater than or equal to 2020")
          @Max(value = 2030, message = "year must be lower than 2050")
          int year) {
    ConferenceRoomUser temp =
        userRepository
            .findByUsernameIs(userName)
            .orElseThrow(() -> new UserNotFoundException("User name not found:" + userName));
    List<Reservation> reservations =
        reservationService.createFeeInvoiceForMonthSingleUser(temp, month, year);
    return calculateInvoice(userName, reservations);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @GetMapping("/team/{teamName}/{month}/{year}")
  ResponseEntity<InvoiceVO> getFeesForTeamAndMonth(
      @PathVariable String teamName,
      @PathVariable
          @Min(value = 1, message = "day must be greater than or equal to 1")
          @Max(value = 31, message = "day must be lower " + "than 31")
          int month,
      @PathVariable
          @Min(value = 2020, message = "year must be greater " + "than or " +
                  "equal to 2020")
          @Max(value = 2050, message = "year must be lower " + "than 2050")
          int year) {
    List<Reservation> reservations =
        reservationService.createFeeInvoiceForMonthTeam(teamName, month, year);

    return calculateInvoice(teamName, reservations);
  }

  private ResponseEntity<InvoiceVO> calculateInvoice(
      String recipient, List<Reservation> reservations) {
    List<ReservationVO> reservationVOList = new ArrayList<>();
    BigDecimal totalFees = BigDecimal.valueOf(0);
    long totalTimeSum = 0;
    String currency = "", tempCurrency = "";
    boolean isSingleCurrency = true;

    List<Reservation> sortedReservationList =
        reservations.stream()
            .sorted(Comparator.comparing(Reservation::getReservationStart))
            .collect(Collectors.toList());

    for (Reservation r : sortedReservationList) {
      ReservationVO rvo = new ReservationVO();

      /* multiply the hourly fee by the reservation time to get the total fees for 1 reservation */
      BigDecimal feePerHour = r.getConferenceRoom().getRoomFee();

      LocalDateTime reservationStart = r.getReservationStart();
      LocalDateTime reservationEnd = r.getReservationEnd();
      long elapsedTime = ChronoUnit.MINUTES.between(reservationStart, reservationEnd);
      totalTimeSum += elapsedTime;

      BigDecimal feePerMinute = feePerHour.divide(new BigDecimal(60), 5, RoundingMode.HALF_DOWN);
      BigDecimal totalFeesForTheTime =
          feePerMinute.multiply(new BigDecimal(elapsedTime)).setScale(2, BigDecimal.ROUND_HALF_UP);
      currency = r.getConferenceRoom().getRoomFeeCurrency();

      /* check if reservations are in the same currency  */
      if (!tempCurrency.isEmpty() && !currency.equals(tempCurrency)) {
        isSingleCurrency = false;
      }
      rvo.setReservationStart(r.getReservationStart().toString());
      rvo.setReservationEnd(r.getReservationEnd().toString());
      rvo.setConfirmationDate(null != r.getConfirmationDate()?
              r.getConfirmationDate().toString():"");
      rvo.setCurrency(currency);
      rvo.setStatus(r.getStatus().toString());
      rvo.setFeePerHour(r.getConferenceRoom().getRoomFee().toString());
      rvo.setTotalFee(totalFeesForTheTime.toString());
      rvo.setReservedBy(r.getConferenceRoomUser().getUsername());
      rvo.setTotalNumberOfMinutes(String.valueOf(elapsedTime));
      reservationVOList.add(rvo);
      totalFees = totalFees.add(totalFeesForTheTime);
      tempCurrency = currency;
    }
    InvoiceVO invoiceVO = new InvoiceVO();
    if (isSingleCurrency) {
      invoiceVO.setSum(totalFees);
      invoiceVO.setCurrency(currency);
      invoiceVO.setTotalTimeInMinutes(totalTimeSum);
    } else {
      invoiceVO.setSum(BigDecimal.valueOf(0));
      invoiceVO.setCurrency(
          "Distinct currencies found, WILL NOT GENERATE "
              + "SUM. Please check each reservation "
              + "value. CURRENCY CONVERTER NOT IMPLEMENTED YET ...");
    }
    invoiceVO.setReservations(reservationVOList);
    invoiceVO.setInvoiceFor(recipient);

    return ResponseEntity.ok(invoiceVO);
  }
}
