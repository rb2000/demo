package tech.rvidal.demo.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.rvidal.demo.domain.*;
import tech.rvidal.demo.exceptions.ResourceNotFoundException;
import tech.rvidal.demo.service.ConferenceRoomService;
import tech.rvidal.demo.service.ConferenceRoomUserService;
import tech.rvidal.demo.service.ReservationService;
import tech.rvidal.demo.utils.DtoConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@Validated
@RequestMapping("/api/v1/rooms")
public class ConferenceRoomController {

  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

  private final ConferenceRoomService conferenceRoomService;
  private final ReservationService reservationService;
  private final ConferenceRoomUserService userService;
  private final DtoConverter dtoConverter;

  public ConferenceRoomController(ConferenceRoomService conferenceRoomService,
                                  ReservationService reservationService,
                                  ConferenceRoomUserService userService,
                                  DtoConverter dtoConverter) {
    this.conferenceRoomService = conferenceRoomService;
    this.reservationService = reservationService;
      this.userService = userService;
      this.dtoConverter = dtoConverter;
  }

  private boolean isRoomAvailableOnDate(String roomNumber, String beginDate, String endDate) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
    LocalDateTime start = LocalDateTime.parse(beginDate, formatter);
    LocalDateTime end = LocalDateTime.parse(endDate, formatter);
    return reservationService.countReservationsWithinDateRangeAndRoomName(roomNumber, start, end)
        == 0;
  }

  @PreAuthorize("hasAnyAuthority('APPLICATION')")
  @GetMapping(path = "/remove/all")
  public ResponseEntity.BodyBuilder cleanDatabase() {
    conferenceRoomService.deleteAllInBatch();
    userService.deleteAllInBatch();
    reservationService.deleteAllInBatch();
    return ResponseEntity.ok();
  }

  @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
  @GetMapping(path = "/all")
  public ResponseEntity<List<ConferenceRoom>> getAllRooms() {
    return conferenceRoomService
        .findAllSorted()
        .map(ResponseEntity::ok)
        .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
  @GetMapping(path = "/find/{floorNumber}/{hasVideo}/{hasDrawingBoard}")
  public ResponseEntity<List<ConferenceRoom>> getReservationByFloorBeamerAndDrawingBoard(
      @PathVariable(value = "floorNumber") int floorNumber,
      @PathVariable(value = "hasVideo") boolean hasVideo,
      @PathVariable(value = "hasDrawingBoard") boolean hasDrawingBoard)
      throws ResourceNotFoundException {
    List<ConferenceRoom> conferenceRoomList =
        conferenceRoomService.getReservationByFloorBeamerAndDrawingBoard(
            floorNumber, hasVideo, hasDrawingBoard);
    return ResponseEntity.ok(conferenceRoomList);
  }

  @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
  @GetMapping("/availability/{flooNumber}/{beginDate}/{endDate}")
  ResponseEntity<List<AvailabilityVO>> getAvailability(
      @PathVariable int flooNumber, @PathVariable String beginDate, @PathVariable String endDate) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
    LocalDateTime start = LocalDateTime.parse(beginDate, formatter);
    LocalDateTime end = LocalDateTime.parse(endDate, formatter);
    List<Reservation> allReservationsInFloor =
        reservationService.findReservationsOnFloorByDate(flooNumber, start, end);

    List<AvailabilityVO> sortedReservationList =
        allReservationsInFloor.stream()
            .sorted(Comparator.comparing(Reservation::getReservationStart))
            .map(
                r -> {
                  AvailabilityVO av = new AvailabilityVO();
                  av.setReservationID(r.getId().toString());
                  av.setRoomFloor(String.valueOf(r.getConferenceRoom().getLocation().getFloor()));
                  av.setRoomNumber(
                      String.valueOf(r.getConferenceRoom().getLocation().getRoomNumber()));
                  av.setReservationStart(r.getReservationStart().format(formatter));
                  av.setReservationEnd(r.getReservationEnd().format(formatter));
                  av.setStatus(r.getStatus().name());
                  av.setConfirmationDate(
                      r.getConfirmationDate() != null
                          ? r.getConfirmationDate().format(formatter)
                          : "");
                  av.setExpireDate(r.getExpireDate().format(formatter));
                  av.setReservedBy(
                      r.getConferenceRoomUser().getUsername()
                          + "(from '"
                          + r.getConferenceRoomUser().getTeamName()
                          + "')");
                  return av;
                })
            .collect(Collectors.toList());
    return ResponseEntity.ok(sortedReservationList);
  }

  @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
  @GetMapping("/book/{roomNumber}/{beginDate}/{endDate}")
  ResponseEntity<ReservationVO> bookConferenceRoom(
      @PathVariable String beginDate, @PathVariable String endDate, @PathVariable String roomNumber)
      throws ResourceNotFoundException {
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      ConferenceRoomUser user = (ConferenceRoomUser) auth.getPrincipal();
      log.info("Booking conference room {} for {}", roomNumber, user.getUsername());
      ConferenceRoom room = conferenceRoomService.findByConferenceRoomByRoomNumber(roomNumber);
      if (null == room) throw new ResourceNotFoundException("There is no room with this number");
      LocalDateTime start = LocalDateTime.parse(beginDate, formatter);
      LocalDateTime end = LocalDateTime.parse(endDate, formatter);
      long timeToLive = start.until(end, ChronoUnit.SECONDS) / 10;

      Reservation temp =
          Reservation.builder()
              .reservationStart(LocalDateTime.parse(beginDate, formatter))
              .reservationEnd(LocalDateTime.parse(endDate, formatter))
              .conferenceRoom(room)
              .conferenceRoomUser(user)
              .expireDate(start.plusSeconds(timeToLive))
              .status(ReservationStatus.PENDING)
              .build();

      Reservation fromDB = reservationService.createReservation(temp);
      return ResponseEntity.ok(dtoConverter.convertToReservationsView(fromDB));
  }
}
