package tech.rvidal.demo.controllers;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.rvidal.demo.domain.ConferenceRoom;
import tech.rvidal.demo.domain.Reservation;
import tech.rvidal.demo.domain.ReservationDTO;
import tech.rvidal.demo.domain.ReservationVO;
import tech.rvidal.demo.exceptions.ResourceNotFoundException;
import tech.rvidal.demo.exceptions.RoomCapacityExceededException;
import tech.rvidal.demo.service.ConferenceRoomService;
import tech.rvidal.demo.service.ReservationService;
import tech.rvidal.demo.utils.DtoConverter;

import javax.validation.Valid;
import java.lang.reflect.Field;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api/v1/reservations")
public class ReservationController {
    private final ReservationService reservationService;
    private final ConferenceRoomService roomService;
    private final DtoConverter dtoConverter;

    public ReservationController(
            ReservationService reservationService,
            ConferenceRoomService roomService,
            DtoConverter dtoConverter) {
        this.reservationService = reservationService;
        this.roomService = roomService;
        this.dtoConverter = dtoConverter;
    }

    @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
    @GetMapping("/all")
    public ResponseEntity<List<ReservationVO>> getAllReservations() {
        List<Reservation> allReservations = reservationService.findALl();
        List<ReservationVO> sortedReservationList =
                allReservations.stream()
                               .map(dtoConverter::convertToReservationsView)
                               .sorted(Comparator.comparing(ReservationVO::getReservationStart))
                               .collect(Collectors.toList());
        return ResponseEntity.ok().body(sortedReservationList);
    }

    @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
    @GetMapping("/fromNow/{floorNumber}")
    public ResponseEntity<List<ReservationVO>> findReservationsFromNowOnForFloor(
            @PathVariable(value = "floorNumber") Integer floorNumber) {
        List<Reservation> allReservations = roomService.checkAvailabilityOnFloorFromNowOn(floorNumber);
        List<ReservationVO> sortedReservationList =
                allReservations.stream()
                               .sorted(Comparator.comparing(Reservation::getReservationStart))
                               .map(dtoConverter::convertToReservationsView)
                               .collect(Collectors.toList());
        return ResponseEntity.ok().body(sortedReservationList);
    }

    @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
    @GetMapping(path = "/{id}")
    public ResponseEntity<ReservationVO> getReservationById(
            @PathVariable(value = "id") Long reservationId) throws ResourceNotFoundException {
        Reservation reservation =
                reservationService
                        .findReservationById(reservationId)
                        .orElseThrow(
                                () -> new ResourceNotFoundException("Reservation not found: " + reservationId));
        return ResponseEntity.ok().body(dtoConverter.convertToReservationsView(reservation));
    }

    @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
    @PostMapping(value = "/reservation", consumes = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseEntity<ReservationVO> saveReservation(@RequestBody String reservationDto) {
        Gson gson = new Gson();
        Reservation reservation = gson.fromJson(reservationDto, Reservation.class);
        log.info("Saving Reservation request: {}", reservation);
        Reservation saved = reservationService
               .createReservation(reservation);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(saved.getId()).toUri();
        return ResponseEntity.created(location).build();
}

    @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<Reservation> saveOrUpdate(
            @PathVariable(value = "id") Long reservationId,
            @Valid @RequestBody Reservation reservationDetails) {
        return reservationService
                .findReservationById(reservationId)
                .map(
                        r -> {
                            r.setReservationStart(reservationDetails.getReservationStart());
                            r.setReservationEnd(reservationDetails.getReservationEnd());
                            r.setConferenceRoom(reservationDetails.getConferenceRoom());
                            r.setConferenceRoomUser(reservationDetails.getConferenceRoomUser());
                            r.setConfirmationDate(reservationDetails.getConfirmationDate());
                            r.setStatus(reservationDetails.getStatus());
                            r.setTotalFeeForTime(reservationDetails.getTotalFeeForTime());
                            r.setExpireDate(reservationDetails.getExpireDate());
                            return ResponseEntity.accepted().body(this.reservationService.save(r));
                        })
                .orElseGet(
                        () -> {
                            return ResponseEntity.accepted()
                                                 .body(this.reservationService.save(reservationDetails));
                        });
    }

    @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN')")
    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteReservation(@PathVariable(value = "id") Long reservationId)
            throws ResourceNotFoundException {
        reservationService
                .findReservationById(reservationId)
                .orElseThrow(
                        () -> new ResourceNotFoundException("Reservation not found: " + reservationId));
        reservationService.deleteReservation(reservationId);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN')")
    @PatchMapping(path = "/{id}", consumes = "application/json")
    ResponseEntity<Reservation> patch(
            @RequestParam String reservationId, @RequestBody Map<String, String> update) {
        try {
            Reservation fromDB =
                    reservationService
                            .findReservationById(Long.parseLong(reservationId))
                            .orElseThrow(
                                    () ->
                                            new ResourceNotFoundException(
                                                    "Reservation not found with id: " + reservationId));
            Reservation customerPatched = applyPatchToReservation(fromDB, update);
            reservationService.updateReservation(
                    Long.parseLong(reservationId), dtoConverter.convertToDto(customerPatched));
            return ResponseEntity.ok(customerPatched);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PreAuthorize("hasAuthority('APPLICATION')")
    @GetMapping("/deleteAllUnconfirmed")
    public void deleteAllUnconfirmed() {
        reservationService.checkIfReservationsTimedOut();
        reservationService.deleteUnconfirmedReservations();
    }

    @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
    @GetMapping("/confirm/{reservationId}")
    public ResponseEntity<Void>  confirmReservation(@PathVariable(value = "reservationId") Long reservationId) {
     reservationService.confirmReservation(reservationId);
     return  ResponseEntity.status(HttpStatus.OK).build();
    }

    @PreAuthorize("hasAnyAuthority('APPLICATION','ADMIN','EMPLOYEE')")
    @GetMapping("/confirm-arrival/{reservationId}/{employeeName}")
    Map<String, Integer> confirmArrivalInRoom(
            @PathVariable(value = "reservationId") Long reservationId,
            @PathVariable(value = "employeeName") String employeeName)
            throws ResourceNotFoundException, RoomCapacityExceededException {
        Reservation fromDB =
                reservationService
                        .findReservationById(reservationId)
                        .orElseThrow(
                                () ->
                                        new ResourceNotFoundException(
                                                "Reservation not found with id: " + reservationId));
        ConferenceRoom temp = fromDB.getConferenceRoom();
        int numberfCurrentUsers = fromDB.getNumberOfAttendants();

        if ((numberfCurrentUsers + 1) > temp.getCapacity()) {
            throw new RoomCapacityExceededException(
                    "Maximal capacity for this room is " + temp.getCapacity());
        }
        Map<String, Integer> response = new HashMap<>();
        fromDB.setNumberOfAttendants(numberfCurrentUsers + 1);
        reservationService.save(fromDB);
        fromDB = reservationService.findReservationById(reservationId).get();
        response.put("Room '" + temp.getLocation().getRoomNumber() + "' Capacity", temp.getCapacity());
        response.put("Nr.of.attendants before", numberfCurrentUsers);
        response.put("Nr.of.attendants with '" + employeeName + "'", fromDB.getNumberOfAttendants());
        return response;
    }

    @PreAuthorize("hasAnyAuthority('APPLICATION')")
    @GetMapping("/timeoutCheck")
    public ResponseEntity<Void> setExpiredReservationsStatus() {
         this.reservationService.findALl().stream()
                               .filter(
                                       r -> {
                                           long timeToLive =
                                                   r.getBookedOn().until(r.getReservationEnd(), ChronoUnit.SECONDS) / 10;
                                           return (r.getBookedOn().plusSeconds(timeToLive).isBefore(
                                                   LocalDateTime.now())
                                                   && r.getStatus().toString().equals("PENDING"));
                                       })
                               .forEach(
                                       r -> {
                                           this.reservationService.setReservationStatusToTimedOut(r.getId());
                                           log.info("Setting reservation " +
                                                            "status to " +
                                                            "TIMED_OUT for " +
                                                            "reservation with" +
                                                            " id {}",
                                                    r.getId());
                                       });
         return  ResponseEntity.status(HttpStatus.OK).build();
    }




    private Reservation applyPatchToReservation(Reservation fromDB, Map<String, String> update) {
        update.forEach(
                (k, v) -> {
                    Field field = ReflectionUtils.findField(Reservation.class, k);
                    assert field != null;
                    field.setAccessible(true);
                    ReflectionUtils.setField(field, fromDB, v);
                });
        return fromDB;
    }
}
