package tech.rvidal.demo.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.rvidal.demo.domain.ConferenceRoomUser;
import tech.rvidal.demo.exceptions.ResourceNotFoundException;
import tech.rvidal.demo.exceptions.UserNotFoundException;
import tech.rvidal.demo.service.ConferenceRoomUserService;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api/v1/users")
public class ConferenceRoomUserController {

    private final ConferenceRoomUserService  conferenceRoomUserService;

    public ConferenceRoomUserController(ConferenceRoomUserService conferenceRoomUserService) {
        this.conferenceRoomUserService = conferenceRoomUserService;
    }


    @GetMapping("/{id}")
    public ResponseEntity<ConferenceRoomUser> getUserById(
            @PathVariable(value = "userId") String userId ) throws ResourceNotFoundException {
        ConferenceRoomUser user =
                conferenceRoomUserService.getUserById(userId)
                        .orElseThrow(
                                () -> new UserNotFoundException("User not found: " + userId));
        return ResponseEntity.ok().body(user);
    }
  @GetMapping("/allusers")
  public ResponseEntity<List<ConferenceRoomUser>> getALlUsers() {

    List<ConferenceRoomUser> allUsers = conferenceRoomUserService.findAll();
    if (allUsers.size() < 2) {
      return ResponseEntity.ok(allUsers);
    } else {
      List<ConferenceRoomUser> sortedUsersList =
          allUsers.stream()
              .sorted(Comparator.comparing(ConferenceRoomUser::getTeamName))
              .collect(Collectors.toList());
      return ResponseEntity.ok(sortedUsersList);
    }
  }

  @RequestMapping(value = "/whoami", method = RequestMethod.GET)
  @ResponseBody
  public String whoami(HttpServletRequest request) {
    Principal principal = request.getUserPrincipal();
    log.info(principal.toString());
    return principal.getName();
  }
}
